# Optical Calibration System (OCS)

This respository contains the code for the Optical Calibration System (OCS). The OCS is an OpenCV powered calibration program that can detect antennas in the ADome.

By controlling LEDs located on the PCBs of the antennas, the OCS is able to determine the pixel location of the LEDs, transform these into spherical angles with respect to the camera.

These angles can than be transformed to coordinates in the ADome by using fiducials located in the ADome.

For developers interested in building applications using the OCS code, refer to the [Recommendations](#recommendations).

## Table of Contents
- [Structure of the repository](#structure-of-the-repository)
  * [Executable](#executable)
  * [Static library](#static-library)
  * [Mex](#mex)
- [Provided binaries](#provided-binaries)
- [Building the code](#building-the-code)
    * [Installing OpenCV on Windows](#installing-opencv-on-windows)
    * [Installing OpenCV for linux](#installing-opencv-for-linux)
    * [Building the executable](#building-the-executable)
    * [Using and building the static library and the mex function](#using-and-building-the-static-library-and-the-mex-function)
- [Recommendations](#recommendations)

## Structure of the repository
The repository is build up to support building 3 different binaries: a simple executable, a static library for using the code in another project, and a mex executable which can be used by Matlab as a function.

### Executable
The code specific to the executable can be found in [`main.cpp`](main.cpp).

### Static library
The code specific to the static library can be found in the [`staticlib`](staticlib) folder. The [`locStat.h`](staticlib/locStat.h) file has to be included in other projects.

### Mex
The code specific to the mex function can be found in the [`mex`](mex) folder.

## Provided binaries
Compiled binaries can be found in the [releases](https://gitlab.com/daan99/adome/-/releases) tab.

## Building the code
It is possible to build the executable, static library, and mex yourself. To do this, it is necessary to have at least version 4.5.3 of OpenCV installed. Next to this, the OpenCV contrib libraries have to be installed as well. It is recommended to install OpenCV as static libraries, as this will allow the binaries to be build without the need of e.g. `.dll` files.

### Installing OpenCV on Windows
For the following instructions, it is assumed that the code is compiled on Windows using Visual Studio.

To install OpenCV on Windows, as similar method is used as explained in [this article](https://docs.opencv.org/4.x/d3/d52/tutorial_windows_install.html). The only difference is that the script to install the OpenCV libaries have been changed to compile into static libraries.

**Installation process**

1. Install git-bash
2. Create a folder for your OpenCV install
3. Open git-bash in this folder
4. Create a file `installOCV.sh` and add the following code to the file. Make sure the `CMAKE_GENERATOR_OPTIONS` variable matches your version of Visual Studio.
```bash
#!/bin/bash -e
myRepo=$(pwd)
CMAKE_GENERATOR_OPTIONS=-G"Visual Studio 16 2019"
if [  ! -d "$myRepo/opencv"  ]; then
    echo "cloning opencv"
    git clone https://github.com/opencv/opencv.git
else
    cd opencv
    git pull --rebase
    cd ..
fi
if [  ! -d "$myRepo/opencv_contrib"  ]; then
    echo "cloning opencv_contrib"
    git clone https://github.com/opencv/opencv_contrib.git
else
    cd opencv_contrib
    git pull --rebase
    cd ..
fi
RepoSource=opencv
mkdir -p build_opencv
pushd build_opencv
CMAKE_OPTIONS=(-DBUILD_PERF_TESTS:BOOL=OFF -DBUILD_TESTS:BOOL=OFF -DBUILD_DOCS:BOOL=OFF  -DWITH_CUDA:BOOL=OFF -DBUILD_EXAMPLES:BOOL=OFF -DINSTALL_CREATE_DISTRIB=ON -D BUILD_opencv_world=OFF -DBUILD_SHARED_LIBS=OFF)
set -x
cmake "${CMAKE_GENERATOR_OPTIONS[@]}" "${CMAKE_OPTIONS[@]}" -DOPENCV_EXTRA_MODULES_PATH="$myRepo"/opencv_contrib/modules -DCMAKE_INSTALL_PREFIX="$myRepo/install/$RepoSource" "$myRepo/$RepoSource"
echo "************************* $Source_DIR -->debug"
cmake --build .  --config debug
echo "************************* $Source_DIR -->release"
cmake --build .  --config release
cmake --build .  --target install --config release
cmake --build .  --target install --config debug
popd
```
5. Run `chmod +x installOCV.sh` and execute the script using `./installOCV.sh`. (This process can take a while)
6. After the installation, the following folders will be of importance:
    - Library directory: `./install/opencv/x64/${version}/staticlib`, `version` will indicate the version of your visual studio (in the case of Visual Studio 16 this `version`=`vc16`). This directory contains all the static libraries.
    - Include directory: `./install/opencv/include`. This directory contains all header files.

**Using OpenCV in Visual Studio**

1. To use the OpenCV libraries in Visual Studio, create a new project, go to `Project->Properties->VC++ Directories`. In the fields `Include Directories` and `Library directories` add the paths to the directories mentioned above.
2. We will now enable the project to be build statically, and add specific libraries to the project. These steps differ a bit for the Debug and Release build.
    - For the debug build: (Make sure `Configuration` is set to `Debug`)
        1. Go to `C/C++` in the properties menu
        2. Go to `Runtime library` and set the value to `Multi-threaded Debug (/MTd)`
        3. Go to `Linker` in the properties menu
        4. Under `Input->Additional Dependencies` add the libraries that you need, for example `opencv_core453d.lib`. Make sure you are selecting the debug libraries (these are the libraries that end in `d.lib`).
    - For the release build: (Make sure `Configuration` is set to `Release`)
        1. Go to `C/C++` in the properties men
        2. Go to `Runtime library` and set the value to `Multi-threaded (/MT)`
        3. Go to `Linker` in the properties menu
        4. Under `Input->Additional Dependencies` add the libraries that you need, for example `opencv_core453.lib`. Make sure you are selecting the release libraries (these are the libraries that don't end in `d.lib`).

### Installing OpenCV for linux
Since the installation and usage of OpenCV for linux differs per distro, no instructions will be given here. Make sure that when you install OpenCV, you make sure you also install the contrib libraries, and install all libraries as static libraries.

### Building the executable
To build the executable, make sure you have followed the steps above to install OpenCV.

1. Create a project for this distro, add `main.cpp`, and all `.cpp` and `.h` files located in the other subfolders. You can leave out the files located in `staticlib` and `mex`.
2. Add at least the following OpenCV libraries (or their debug versions which end in `d.lib`) to the project (make sure the libraries match the version of your OpenCV, as these libraries are for version 4.5.3): 
```
opencv_core453.lib
opencv_imgcodecs453.lib
opencv_highgui453.lib
opencv_imgproc453.lib
opencv_videoio453.lib
IlmImf.lib
ippicvmt.lib
libjpeg-turbo.lib
libpng.lib
libtiff.lib
libwebp.lib
zlib.lib
ippiw.lib
ittnotify.lib
libopenjp2.lib
opencv_aruco453.lib
opencv_calib3d453.lib
opencv_flann453.lib
opencv_features2d453.lib
```
3. Build your project

### Using and building the static library and the mex function
For more info on using and building the static library and the mex function, please refer to the readmes located in the respective directories.

## Recommendations
When the OCS has to be used in other code, it is highly recommended to write your code using the compiled static library ([staticOCS.lib and locStat.h](https://gitlab.com/daan99/adome/-/releases)).
