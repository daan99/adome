#include "serialComms.h"
using namespace conSer;

// OS Specific implementations
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
   // Windows implementations
   HANDLE conSer::connectPort(std::string port)
    {
        port = std::string("\\\\.\\") + port;
        HANDLE hSerial;

        hSerial = CreateFileA(
            port.c_str(), GENERIC_READ | GENERIC_WRITE,
            0,
            0,
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL,
            0
        );

        if (hSerial == INVALID_HANDLE_VALUE) {
            if (GetLastError() == ERROR_FILE_NOT_FOUND) {
                debugError("Port does not exist!", 2);
            }
            return NULL;
        }

        DCB dcbSerialParams = { 0 };

        dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
        if (!GetCommState(hSerial, &dcbSerialParams)) {
            debugError("Error getting state!", 2);
            return NULL;
        }

        dcbSerialParams.BaudRate = CBR_19200;
        dcbSerialParams.ByteSize = 8;
        dcbSerialParams.StopBits = ONESTOPBIT;
        dcbSerialParams.Parity = NOPARITY;
        if (!SetCommState(hSerial, &dcbSerialParams)) {
            debugError("Error getting state!", 2);
            return NULL;
        }
        PurgeComm(
            hSerial,
            PURGE_RXABORT | PURGE_RXCLEAR | PURGE_TXABORT | PURGE_TXCLEAR
        );

        return hSerial;
    }

    int conSer::setTimeouts(HANDLE hSerial, int timeoutConstant, int timeoutMultiplier) {
        COMMTIMEOUTS timeouts = { 0 };

        timeouts.ReadIntervalTimeout = timeoutConstant;
        timeouts.ReadTotalTimeoutConstant = timeoutConstant;
        timeouts.ReadTotalTimeoutMultiplier = timeoutMultiplier;
        timeouts.WriteTotalTimeoutConstant = timeoutConstant;
        timeouts.WriteTotalTimeoutMultiplier = timeoutMultiplier;
        
        if (!SetCommTimeouts(hSerial, &timeouts)) {
            debugError("Error setting timeouts!", 2);
            return 0;
        }

        return 1;
    }

    int conSer::readData(HANDLE hSerial, char* dataBuffer, int dataLength) {
        DWORD BytesRead = 0;

        if (!ReadFile(hSerial, dataBuffer, dataLength, &BytesRead, NULL)) {
            debugError("Failed to read data!", 2);
            return 0;
        }

        return (int)BytesRead;
    }

    int conSer::writeData(HANDLE hSerial, char* dataBuffer, int dataLength) {
        DWORD BytesWritten = 0;

        if (!WriteFile(hSerial, dataBuffer, dataLength, &BytesWritten, NULL)) {
            debugError("Failed to write data!", 2);
            return 0;
        }

        return (int)BytesWritten;
    }
#elif __linux__ || __unix__
    HANDLE conSer::connectPort(std::string port) {
        port = "/dev/" + port;

        HANDLE handle;

        handle.serial_connection = open(port.c_str(), O_RDWR);

        // Check whether connected to port
        if(handle.serial_connection < 0) {
            debugError((std::string("Error connecting to port: ") + std::string(strerror(errno))).c_str(), 2);
        }

        // Initialize the termios structure
        if(tcgetattr(handle.serial_connection, &handle.tty) != 0) {
            debugError((std::string("Error initializing connection: ") + std::string(strerror(errno))).c_str(), 2);
        }

        // Update control settings
        // Clear the parity bit
        handle.tty.c_cflag &= ~PARENB;

        // Set to use 1 stop bit
        handle.tty.c_cflag &= ~CSTOPB;

        // Set byte size
        handle.tty.c_cflag &= ~CSIZE; // Clear all the size bits
        handle.tty.c_cflag |= CS8;

        // Disable flow control
        handle.tty.c_cflag &= ~CRTSCTS;

        // Setting to read data
        handle.tty.c_cflag |= CREAD | CLOCAL;

        // Update local settings
        // Disable reading line by line
        handle.tty.c_lflag &= ~ICANON;

        // Disable echoes
        handle.tty.c_lflag &= ~ECHO; // Disable echo
        handle.tty.c_lflag &= ~ECHOE; // Disable erasure
        handle.tty.c_lflag &= ~ECHONL; // Disable new-line echo

        // Disable signal characters
        handle.tty.c_lflag &= ~ISIG;

        // Update input settings
        // Turn of software flow control
        handle.tty.c_iflag &= ~(IXON | IXOFF | IXANY);

        // Disable any special handling of received bytes
        handle.tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL);

        // Update output settings
        // Disable output interpretation
        handle.tty.c_oflag &= ~OPOST;
        // Disable newline conversion
        handle.tty.c_oflag &= ~ONLCR;

        // Set baudrates
        cfsetispeed(&handle.tty, B19200);
        cfsetospeed(&handle.tty, B19200);

        // Save the current settings
        if (tcsetattr(handle.serial_connection, TCSANOW, &handle.tty) != 0) {
            debugError((std::string("Error saving configuration: ") + std::string(strerror(errno))).c_str(), 2);
        }

        return handle;
    }

    int conSer::setTimeouts(HANDLE hSerial, int timeoutConstant, int timeoutMultiplier) {
        int timeoutConstantDeci = timeoutConstant/100;

        if(timeoutConstantDeci > 255) {
            debugError("'timeoutConstant' cannot exceed 255 deciseconds (25500 milliseconds)!", 2);
            return 0;
        }

        // Set the timeout of the connection, after 'timeoutConstantDeci' deciseconds, 
        // the data will be returned. There is no minimum amount of characters.
        hSerial.tty.c_cc[VTIME] = timeoutConstantDeci;
        hSerial.tty.c_cc[VMIN] = 0;

        // Save the current settings
        if (tcsetattr(hSerial.serial_connection, TCSANOW, &hSerial.tty) != 0) {
            debugError((std::string("Error setting timeout: ") + std::string(strerror(errno))).c_str(), 2);
            return 0;
        }

        return 1;
    }

    int conSer::readData(HANDLE hSerial, char* dataBuffer, int dataLength) {
        return (int)read(hSerial.serial_connection, &dataBuffer, dataLength);
    }

    int conSer::writeData(HANDLE hSerial, char* data, int dataLength) {
        return (int)write(hSerial.serial_connection, data, dataLength);
    }
#else
#   error "This compiler is not supported! Supported compilers include: Windows and Linux/Unix"
#endif

