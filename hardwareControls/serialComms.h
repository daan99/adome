#pragma once
#include <stdlib.h>
#include <iostream>
#include <string>

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
   #include <Windows.h>
#elif __linux__ || __unix__
    // linux
	#include <fcntl.h> // Contains file controls like O_RDWR
	#include <errno.h> // Error integer and strerror() function
	#include <termios.h> // Contains POSIX terminal control definitions
	#include <unistd.h> // write(), read(), close()

	//typedef int HANDLE;
	struct HANDLE {
		int serial_connection;
		termios tty;
	};
#else
#   error "This compiler is not supported! Supported compilers include: Windows and Linux/Unix"
#endif

#include "../debug/debug.h"
//#include <cstdlib>

namespace conSer
{
	/// <summary>
	/// Connects to the given serial port
	/// </summary>
	/// <param name="port">Name of serial port, in form: "COM0"</param>
	/// <returns>Windows HANDLE to the wanted COM port</returns>
	HANDLE connectPort(std::string port);

	/// <summary>
	/// Sets the timeouts for sending and receiving serial messages
	/// </summary>
	/// <param name="hSerial">Windows HANDLE of the connected serial port</param>
	/// <param name="timeoutConstant">Default timeout value</param>
	/// <param name="timeoutMultiplier">Multiplier for each timeout</param>
	/// <returns>Integer describing succes</returns>
	int setTimeouts(HANDLE hSerial, int timeoutConstant, int timeoutMultiplier);

	/// <summary>
	/// Reads the serial port for data
	/// </summary>
	/// <param name="hSerial">Windows HANDLE of the connected serial port</param>
	/// <param name="dataBuffer">Databuffer used to store the data</param>
	/// <param name="dataLength">Maximum datalength to read</param>
	/// <returns>The length of data that has been read</returns>
	int readData(HANDLE hSerial, char *dataBuffer, int dataLength);

	/// <summary>
	/// Writes data to serial port
	/// </summary>
	/// <param name="hSerial">Windows HANDLE of the connected serial port</param>
	/// <param name="data">Data to send to the serial port</param>
	/// <param name="dataLength">Amount of data to send to the serial port</param>
	/// <returns>Amount of data that has been succesfully send</returns>
	int writeData(HANDLE hSerial, char *data, int dataLength);
}