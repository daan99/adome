#include "ledControl.h"
#define ERR_NODE(nn) if(!connectNode(nn)) return false;
#define RETFALSE delete[] command; return false;

#if __linux__ || __unix__
	#define Sleep(milliseconds) sleep(milliseconds/1000)
#endif

Led::Led(const char* port, int ID, bool printNotifs) : port{ port }, id(ID), printNotifs{ printNotifs } {
	commChain(COMM_CONNECT);

	chainLength();
	if (id >= _chainLength) {
		connected = false;
		std::string chainError = "Error: tried to access a device with ID outside of id range: id ";
		chainError += std::to_string(id) + std::string(", chain-length: ") + std::to_string(_chainLength);

		debugError(chainError.c_str(), 3);
		return;
	}

	connectNode(id);

	getState();
}

bool Led::connectNode(int nodeNum) {
	char* command = new char[50];
	sprintf(command, "N%i;\0", nodeNum);
	return serialSend(command);
}

bool Led::serialSend(char* &command, bool hasData) {
	/*	READ HERE FOR SERIAL UPDATE
	*
	*	Update the read/write commands here 
	*	
	*/

	// Send stuff
	// Check for 'S' or 'E....'

	int dLen = 0;
	for (int c = 0; c < 50; c++) {
		if (command[c] == '\0') break;
		dLen++;
	}

	conSer::writeData(hSerial, command, dLen);

	char buf[50] = { 0 };

	int read = conSer::readData(hSerial, buf, 50);
	if (read == 0) {
		debugError("No data over serial! (-1)", 3);
		return false;
	}

	if (buf[0] == 'E') {
		// 0: Incorrect or unknown command
		// 1: node out of range
		// 2: incorrect length

		std::string serErr;
		switch (buf[1]) {
		case '0':
			serErr += "Incorrect/unknown command! (0)\n";
			serErr += "\tCommand send: ";
			serErr += command;
			
			debugError(serErr.c_str(), 3);
			break;
		case '1':
			std::cout << "ERROR: Node ID out of range! (1)" << std::endl;
			serErr += "Node ID out of range! (1)";

			debugError(serErr.c_str(), 3);
			break;
		case '2':
			serErr += "Incorrect length of command! (2)";
			
			debugError(serErr.c_str(), 3);
			break;
		}
		return false;
	}

	if (hasData) {
		for (int c = 0; c < read; c++) {
			command[c] = buf[c];
			if (command[c] == ';') {
				command[c + 1] = '\0';
				break;
			}
		}
	}

	return true;
}
bool Led::serialSend(const char* command) {
	/*	READ HERE FOR SERIAL UPDATE
	*
	*	Update the read/write commands here
	*
	*/

	// Send stuff
	// Check for 'S' or 'E....'
	int dLen = 0;
	for (int c = 0; c < 50; c++) {
		if (command[c] == '\0') break;
		dLen++;
	}

	conSer::writeData(hSerial, (char*)command, dLen);

	char buf[50] = { 0 };

	int read = conSer::readData(hSerial, buf, 50);
	if (read == 0) {
		debugError("No data over serial! (-1)", 3);
		return false;
	}

	if (buf[0] == 'E') {
		// 0: Incorrect or unknown command
		// 1: node out of range
		// 2: incorrect length
		std::string serErr;
		switch (buf[1]) {
		case '0':
			serErr += "Incorrect/unkown command! (0)\n";
			serErr += "\tCommand send: ";
			serErr += command;
			debugError(serErr.c_str(), 3);
			break;
		case '1':
			debugError("Node ID out of range! (1)", 3);
			break;
		case '2':
			debugError("Incorrect length of command! (2)", 3);
			break;
		}
		return false;
	}

	return true;
}

bool Led::commChain(COMMType comm) {
	char* command;
	switch (comm) {
	case COMM_CHAINLENGTH:
		// TODO: get chainlength
		command = new char[4];
		strcpy(command, "C?;");
		if (serialSend(command, true)) {
			(void)sscanf(command, "C%i;", &this->_chainLength);
		}
		else RETFALSE;
		break;
	case COMM_RGB:
		ERR_NODE(id);
		// TODO: get RGB
		command = new char[4];
		strcpy(command, "L?;");
		if (serialSend(command, true)) {
			int R, G, B;
			(void)sscanf(command, "L%i,%i,%i;", &R, &G, &B);
			this->RGB = cv::Scalar((double)R,(double)G,(double)B);
		}
		else RETFALSE;
		break;
	case COMM_POWER:
		ERR_NODE(id);
		// TODO: get power
		command = new char[4];
		strcpy(command, "P?;");
		if (serialSend(command, true)) {
			(void)sscanf(command, "P%i;", (int*)&this->power);
		}
		else RETFALSE;
		break;
	case COMM_CONNECT:
		// TODO: connect
		command = new char[1];
		hSerial = conSer::connectPort(std::string(port));
		conSer::setTimeouts(hSerial, 50, 10);
		Sleep(2000);
		break;
	default:
		return false;
	}

	delete[] command;

	return true;
}
bool Led::commChain(COMMType comm, bool data) {
	switch (comm) {
	case COMM_CHAINLENGTH:
		return false;
		break;
	case COMM_RGB:
		return false;
		break;
	case COMM_POWER:
	{
		ERR_NODE(id);
		char* command = new char[4];
		sprintf(command, "P%i;", (int)data);
		bool ret = serialSend(command);
		delete[] command;
		return ret;
		break;
	}
	case COMM_CONNECT:
		return false;
		break;
	default:
		return false;
	}

	return true;
}
bool Led::commChain(COMMType comm, int data) {
	switch (comm) {
	case COMM_CHAINLENGTH:
		return false;
		break;
	case COMM_RGB:
		return false;
		break;
	case COMM_POWER:
	{
		ERR_NODE(id);
		char* command = new char[4];
		sprintf(command, "P%i;", data);
		bool ret = serialSend(command);
		delete[] command;
		return ret;
		break;
	}
	case COMM_CONNECT:
		return false;
		break;
	default:
		return false;
	}

	return true;
}
bool Led::commChain(COMMType comm, cv::Scalar data) {
	switch (comm) {
	case COMM_CHAINLENGTH:
		return false;
		break;
	case COMM_RGB:
	{
		ERR_NODE(id);
		char* command = new char[14];
		sprintf(command, "L%i,%i,%i;\0", (int)data[0], (int)data[1], (int)data[2]);
		bool ret = serialSend(command);
		delete[] command;
		return ret;
		break;
	}
	case COMM_POWER:
		return false;
		break;
	case COMM_CONNECT:
		return false;
		break;
	default:
		return false;
	}

	return true;
}

void Led::getState() {
	commChain(COMM_POWER);
	commChain(COMM_RGB);

	RGB = cv::Scalar(0, 0, 0);
	power = true;
}

void Led::info() {
	std::cout << "Led control class" << std::endl;
	std::cout << "\tConnection info" << std::endl;
	std::cout << "\t\tPort: " << port << std::endl;
	std::cout << "\t\tStatus: " << (connected ? "connected" : "disconnected") << std::endl;
	if (!connected) return;
	std::cout << "\t\tId selected: " << id << std::endl;
	std::cout << "\t\tDevices in chain: " << _chainLength << std::endl << std::endl;
	std::cout << "\tState info" << std::endl;
	getState();
	std::cout << "\t\tColor: [" << RGB[0] << "," << RGB[1] << "," << RGB[2] << "]" << std::endl;
	std::cout << "\t\tPower: " << (power ? "on" : "off") << std::endl;
}

void Led::setId(int ID) {
	if (id >= _chainLength) {
		connected = false;
		std::string idErr = "Tried to access a device with ID outside of id range: id ";
		idErr += std::to_string(id);
		idErr += ", chain-length: ";
		idErr += std::to_string(_chainLength);
		debugError(idErr.c_str(), 3);
		return;
	}

	setPower(false);

	// TODO: communicate with arduino on connection

	id = ID;
}

const int Led::getId() { return id; }
const int Led::chainLength() { 
	commChain(COMM_CHAINLENGTH);

	return _chainLength;
}

const bool Led::getPower() {
	getState();

	return power;
}

const cv::Scalar Led::getRGB() {
	getState();

	return RGB;
}

void Led::setColor(const cv::Scalar rgb) {
	commChain(COMM_RGB, rgb);

	RGB = rgb;
}

void Led::setPower(const bool pwr) {
	commChain(COMM_POWER, pwr);

	power = pwr;
}

const bool Led::toggle() {
	commChain(COMM_POWER, !power);

	power = !power;

	return power;
}