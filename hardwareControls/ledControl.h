#pragma once

// TEMPORARY CHANGE ME
#include "serialComms.h"

#include <iostream>
#include <opencv2/core.hpp>
#include <stdlib.h>

#include "../debug/debug.h"

enum COMMType {
	COMM_CHAINLENGTH,
	COMM_RGB,
	COMM_POWER,
	COMM_CONNECT
};

/// <summary>
/// Class to connect to chain that controls LEDs of different nodes
/// </summary>
class Led {
private:
	bool printNotifs;

	// Chain data
	const char* port;
	unsigned int _chainLength;
	unsigned int id;
	bool connected = true;

	// TEMPORARY CHANGE ME
	HANDLE hSerial;
	
	// Color data
	cv::Scalar RGB;
	bool power;

	/// <summary>
	/// Connects to the given node ID
	/// </summary>
	/// <param name="nodeNum">ID of node to connect to</param>
	/// <returns>true when no errors occured</returns>
	bool connectNode(int nodeNum);
	
	/// <summary>
	/// Send serial data and check for errors. If hasData is true, command will store the returned data
	/// </summary>
	/// <param name="command">Command to send over serial</param>
	/// <param name="hasData">Whether this command requests data</param>
	/// <returns>true when no errors occured</returns>
	bool serialSend(char* &command, bool hasData=false);

	/// <summary>
	/// Send serial data and check for errors
	/// </summary>
	/// <param name="command">Command to send</param>
	/// <returns>true when no errors occured</returns>
	bool serialSend(const char* command);

	

	/// <summary>
	/// Send a Communication message to the chain
	/// </summary>
	/// <param name="comm">Communication type</param>
	/// <returns>Succes of communication</returns>
	bool commChain(COMMType comm);

	/// <summary>
	/// Send a Communication message to the chain
	/// </summary>
	/// <param name="comm">Communication type</param>
	/// <param name="data">Boolean to send to chain</param>
	/// <returns>Succes of communication</returns>
	bool commChain(COMMType comm, bool data);

	/// <summary>
	/// Send a Communication message to the chain
	/// </summary>
	/// <param name="comm">Communication type</param>
	/// <param name="data">Integer to send to chain</param>
	/// <returns>Succes of communication</returns>
	bool commChain(COMMType comm, int data);

	/// <summary>
	/// Send a Communication message to the chain
	/// </summary>
	/// <param name="comm">Communication type</param>
	/// <param name="data">Scalar to send to chain</param>
	/// <returns>Succes of communication</returns>
	bool commChain(COMMType comm, cv::Scalar data);

	/// <summary>
	/// Get current state data of the selected node, and the chain
	/// </summary>
	void getState();
public:
	Led(const char* port, int ID=0, bool printNotifs=true);

	/// <summary>
	/// Print info of the connection
	/// </summary>
	void info();

	// Chain methods
	
	/// <summary>
	/// Select a new node to connect to
	/// </summary>
	/// <param name="ID">Id of node to connect to</param>
	void setId(int ID);
	/// <summary>
	/// Get ID of current node
	/// </summary>
	/// <returns>ID of node</returns>
	const int getId();
	/// <summary>
	/// Get the chainlength of the chain
	/// </summary>
	/// <returns>Chainlength</returns>
	const int chainLength();

	// Color methods
	
	/// <summary>
	/// Get current power state of selected node
	/// </summary>
	/// <returns>Power state</returns>
	const bool getPower();
	/// <summary>
	/// Get current selected RGB color on the selected node
	/// </summary>
	/// <returns>RGB Scalar</returns>
	const cv::Scalar getRGB();
	/// <summary>
	/// Set a new RGB value to the selected node
	/// </summary>
	/// <param name="rgb">New RGB value</param>
	void setColor(const cv::Scalar rgb);
	/// <summary>
	/// Set a new power state on the selected node
	/// </summary>
	/// <param name="pwr">New power state</param>
	void setPower(const bool pwr);
	/// <summary>
	/// Toggle the power state on the selected node
	/// </summary>
	/// <returns>New power state</returns>
	const bool toggle();
};