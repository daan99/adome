# OCS MEX
For direct usage of the OCS in Matlab, code has been written that supports running the OCS code as a Matlab function. This is done by using Matlab's MEX framework.

A pre-compiled version of the MEX function is provided in the [releases](https://gitlab.com/daan99/adome/-/releases) tab, but this version is only supported in Matlab for Windows.

## Table of Contents
- [Building the MEX](#building-the-mex)
  * [Building using a supported compiler](#building-using-a-supported-compiler)
  * [Building using the mex command](#building-using-the-mex-command)
- [Using the MEX function](#using-the-mex-function)

## Building the MEX
In case the MEX function has to be used in Matlab for linux of MacOS, or the user wants to change the MEX method, it is possible to build the MEX method. 

For this instruction, it is assumed that the user has access to the static OCS library, OpenCV, Matlab, and the Matlab SDK. 

For more detailed instructions on building MEX functions and extra functionalities of MEX functions, refer to [the Matlab page](https://www.mathworks.com/help/matlab/cpp-mex-file-applications.html). Now follows a short summary.

The application can be build in two ways: using a [supported compiler](https://www.mathworks.com/support/requirements/supported-compilers.html), or the `mex` Matlab command.

### Building using a supported compiler
Make sure your project is setup as follows:
- The compiler builds a dynamic library (`.dll` on Windows)
- The compiler outputs a mex file (`.mexw64` on Windows)
- The compiler includes the Matlab headers and libraries (these are located at `C:\Program Files\MATLAB\${version}\extern\include` and `C:\Program Files\MATLAB\${version}\extern\lib\win64\microsoft` on Windows)
- The following Matlab libraries are linked:
```
libmx.lib
libmex.lib
libmat.lib
libMatlabDataArray.lib
libMatlabEngine.lib
```
- The compiler includes the `locStat.h` and `staticOCS.lib`.
- Make sure the following OpenCV libraries are linked:
```
opencv_core453.lib
opencv_imgcodecs453.lib
opencv_highgui453.lib
opencv_imgproc453.lib
opencv_videoio453.lib
IlmImf.lib
ippicvmt.lib
libjpeg-turbo.lib
libpng.lib
libtiff.lib
libwebp.lib
zlib.lib
ippiw.lib
ittnotify.lib
libopenjp2.lib
opencv_aruco453.lib
opencv_calib3d453.lib
opencv_flann453.lib
opencv_features2d453.lib
```

When setup correctly, the mex file can be compiled.

### Building using the mex command
Matlab itself has a mex command to build the mex file. More can be found [here](https://www.mathworks.com/help/matlab/cpp-mex-file-applications.html).

## Using the MEX function
To use the mex function, do the following:
1. Open the folder where the mex file is located (ending with `.mexw64` on Windows).
2. The name of the mex file can be called in Matlab as a function, this will call the MEX file. For example, for the pre-compiled version `locateAntennas.mexw64`, you have to call `locateAntennas`.
3. The function can then be called as follows:
```matlab
json = locateAntennas(COMPort, numberOfAntennas, numberOfLedsPerAntenna, numberOfTrials)
```
