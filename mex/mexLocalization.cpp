/* localization
 * Locates antennas in the adome
 * 
 * locations = localization(COMPort,numberOfAntennas,numberOfLeds,trials,...optional arguments);
*/

#include <locStat.h>
#include <iostream>
#include <mex.hpp>
#include <mexAdapter.hpp>
#include <uchar.h>

#include "../outputJSON/outputJSON.h"

using namespace cv;
using namespace matlab::data;
using matlab::mex::ArgumentList;

typedef void (*ErrCallback) (int);
static std::shared_ptr<matlab::engine::MATLABEngine> matlabPtr;

// Helper functions

/// <summary>
/// Get a matlab string from the input arguments
/// </summary>
/// <param name="arguments">The input arguments list</param>
/// <param name="index">Index of the argument containing the string</param>
/// <returns>matlab::data::String == matlab::data::MATLABString == std::basic_string(uchar16_t)</returns>
matlab::data::String getStringArg(matlab::mex::ArgumentList arguments, int index) {
    matlab::data::TypedArray<matlab::data::MATLABString> strArr = arguments[index];

    matlab::data::String str = strArr[0];

    return str;
}

/// <summary>
/// Transform a matlab string (uchar16_t) to a normal string (char)
/// </summary>
/// <param name="char16String">The uchar16_t string</param>
/// <returns>A normal char string</returns>
std::string toCharString(matlab::data::String& char16String) {
    std::string char8String;
    for (int c = 0; c < char16String.size(); c++) {
        mbstate_t st;
        char char8;
        (void)c16rtomb(&char8, char16String[c], &st);
        char8String += char8;
    }

    return char8String;
}

matlab::data::String toChar16String(std::string& charString) {
    matlab::data::String char16String;
    for (int c = 0; c < charString.size(); c++) {
        mbstate_t st;
        char16_t char16;
        (void)mbrtoc16(&char16, &charString[c], 1, &st);
        char16String += char16;
    }

    return char16String;
}

// Main Functionality

// Stream class used to redirect std::cout to matlab
class mystream : public std::streambuf
{
protected:
    std::ostringstream stream;
    ArrayFactory factory;
    virtual std::streamsize xsputn(const char* s, std::streamsize n) { 
        stream << s;
        
        matlabPtr->feval(u"fprintf", 0,
            std::vector<Array>({ factory.createScalar(stream.str()) }));
        stream.str("");
        return n; 
    }
    virtual int overflow(int c = EOF) { 
        if (c != EOF) {
            matlabPtr->feval(u"fprintf", 0,
                std::vector<Array>({ factory.createScalar(std::string(1,c)) }));
        } 
        return 1; 
    }
public:
};
// Class to create redirection
mystream mout;
class scoped_redirect_cout
{
public:
    scoped_redirect_cout() { old_buf = std::cout.rdbuf(); std::cout.rdbuf(&mout); }
    ~scoped_redirect_cout() { std::cout.rdbuf(old_buf); }
private:
    std::streambuf* old_buf;
};
static scoped_redirect_cout mycout_redirect;

// Main mex function
class MexFunction : public matlab::mex::Function {
private:
    // Settings
    bool debugMode = false;
public:
	void operator()(ArgumentList outputs, ArgumentList inputs) {
        matlabPtr = getEngine();                                    // Assign ptr to matlab engine
        ArrayFactory factory;                                       // Factory for arrays

        ErrCallback errCallback= [](int exitCode) {                 // Create callback function for errors
            std::string err = "Localization failed, exited with code: ";
            err += std::to_string(exitCode);

            // Close the video capture object, otherwise the process will never stop completely.
            locStat::onExit();

            ArrayFactory factory;
            matlabPtr->feval(u"error",
                0,
                std::vector<Array>({
                        factory.createScalar(err.c_str())
                    })
            );
        };

        // Set the callback for when an error occurs
        locStat::setDebugExitCallback(errCallback);

        if (!verifyArguments(errCallback, outputs, inputs)) return;

        // Get all arguments
        std::string comPort = toCharString(getStringArg(inputs, 0));
        double noAntennas = inputs[1][0];
        double noLeds = inputs[2][0];
        double noTrials = inputs[3][0];
        debugMode = inputs[4][0];

        // Call the locate function from the localisation static library
        JSON json = locStat::locate((char*)comPort.c_str(), (int)noAntennas, (int)noLeds, (int)noTrials, debugMode);
        
        // Get json in string format
        std::string jsonstring;
        json.toString(jsonstring);

        // Create dimensions for the output array, size is 1 since only 1 value
        matlab::data::ArrayDimensions aDim;
        aDim.push_back(1);

        // Create a MATLABString array using created dimensions
        matlab::data::TypedArray<matlab::data::MATLABString> strArr = factory.createArray<matlab::data::MATLABString>(aDim);
        
        // Convert the UTF8 json string to UTF16 and assign the json string to the string array
        strArr[0] = toChar16String(jsonstring);

        // Assign the string array to the outputs
        outputs[0] = strArr;
    }

    bool verifyArguments(ErrCallback err, ArgumentList& outputs, ArgumentList& inputs) {
        if (outputs.size() != 1) {
            // TODO ability to use debug library in here maybe?
            std::cout << "Incorrect amount of output arguments. Arguments received: ";
            std::cout << outputs.size() << ", arguments needed: 1";
            std::cout << std::endl;
            err(1);
            return false;
        }
        if (inputs.size() < 4) {
            std::cout << "At least 4 input arguments required, only "; 
            std::cout << inputs.size();
            std::cout << " received!";
            std::cout << std::endl;
            err(1);
            return false;
        }
        if (
            inputs[0].getType() != ArrayType::MATLAB_STRING ||
            inputs[1].getType() != ArrayType::DOUBLE ||
            inputs[2].getType() != ArrayType::DOUBLE ||
            inputs[3].getType() != ArrayType::DOUBLE
            ) {
            std::cout << "Incorrect argument types, required arguments:";
            std::cout << std::endl;
            std::cout << "\t- COM port of ADome connection\n\t- Number of antennas in the ADome\n\t- Number of leds per antenna\n\t- Number of trials";
            std::cout << std::endl;
            err(1);
            return false;
        }
        // TODO(Daan) parse optional arguments
        return true;
    }
};