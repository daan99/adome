#include "debug.h"

static char **_rootPath = nullptr;
static int _rootPathDepth = 0;
static bool _debugLogging = false;
static char *_debugLoggingPath = nullptr;
static std::ofstream *_logStream = nullptr;
static DebugExitCallback _onExit = nullptr;

// Create linux alternatives for Windows.h's safe functions
#if __linux__ || __unix__
	#warning("The program is compiled with unsafe strnlen_s and strcpy_s alternatives!");

	#define strnlen_s(str, maxLength) strlen(str);
	#define strcpy_s(dest, size, src) strcpy(dest, src);
#endif

/// <summary>
/// Seperate a path into an array of folders/files, with an integer storing the depth
/// </summary>
/// <param name="fullPath">The character array to the path of the file</param>
/// <param name="length">The length of the character array, the value will be changed to the depth of the path</param>
/// <returns>The array of folders/files</returns>
char **getPath(char *fullPath, int &length)
{
	char **pathArray;
	int pathLength = length;
	length = 0;

	// Scan for / or \ in the string to find the depth
	for (int i = 0; i < pathLength; i++)
		if (fullPath[i] == '/' || fullPath[i] == '\\')
			length++;

	length++;
	// Create the array holding all the folders/files
	int lastLength = 0;
	pathArray = new char *[length];
	int curDepth = 0;

	// Extract all filenames/foldernames
	for (int i = 0; i < pathLength; i++)
	{
		// Keep track of the length of the current filename/foldername
		if (fullPath[i] == '/' || fullPath[i] == '\\')
		{
			// Create a character array inside the array of files/folders
			pathArray[curDepth] = new char[lastLength + 1];
			pathArray[curDepth][lastLength] = '\0';
			int c = 0;
			// Copy the filename/foldername into the newly made character array
			for (int s = i - lastLength; s < i; s++)
				pathArray[curDepth][c++] = fullPath[s];

			// When the current depth is one before the final, just copy the final word into the array
			if (++curDepth == length - 1)
			{
				pathArray[curDepth] = new char[pathLength - i];
				pathArray[curDepth][pathLength - i - 1] = '\0';

				int c = 0;
				for (int s = i + 1; s < pathLength; s++)
					pathArray[curDepth][c++] = fullPath[s];

				break;
			}
			lastLength = 0;
		}
		else
			lastLength++;
	}

	return pathArray;
}

/// <summary>
/// Extract the depth and filenames/foldernames from the path of int main and store them in static global variables
/// </summary>
/// <param name="callerPath">character array containing the path to the main file containing int main</param>
void setRootPath(char *callerPath)
{
	int pathLength = (int)strlen(callerPath);

	_rootPath = getPath(callerPath, pathLength);

	_rootPathDepth = pathLength;
}

// Functions to enable/disable saving of the debug logs in a file
void _setDebugLogging(bool loggingState, const char *callerPath, const char *callFunction)
{
	if (strcmp(callFunction, "int main()") != 0 && strcmp(callFunction, "int main(int, char**)") != 0 && strcmp(callFunction, "int __cdecl main(int,char *[])") != 0 && strcmp(callFunction, "int __cdecl main()") != 0 && strcmp(callFunction, "class JSON &__cdecl locStat::locate(char *,int,int,int,bool)") != 0)
	{
		std::cout << callFunction << std::endl;
		std::cout << "setDebugLogging should only be called in the main function!" << std::endl;
		throw("setDebugLogging should only be called in the main function!");
	}

	if (_rootPath == nullptr)
		setRootPath((char *)callerPath);

	_debugLogging = loggingState;
}

void _setDebugPath(char *loggingPath, const char *callerPath, const char *callFunction)
{
	if (strcmp(callFunction, "int main()") != 0 && strcmp(callFunction, "int main(int, char**)") != 0 && strcmp(callFunction, "int __cdecl main(int,char *[])") != 0 && strcmp(callFunction, "int __cdecl main()") != 0 && strcmp(callFunction, "class JSON &__cdecl locStat::locate(char *,int,int,int,bool)") != 0)
	{
		std::cout << "setDebugPath should only be called in the main function!" << std::endl;
		throw("setDebugPath should only be called in the main function!");
	}

	if (_rootPath == nullptr)
		setRootPath((char *)callerPath);

	if (_debugLoggingPath != nullptr)
	{
		delete[] _debugLoggingPath;
		delete _logStream;
	}

	int pathSize = (int)strnlen_s(loggingPath, 100);
	_debugLoggingPath = new char[pathSize + 1];
	strcpy_s(_debugLoggingPath, pathSize + 1, loggingPath);

	_logStream = new std::ofstream(_debugLoggingPath, std::ofstream::out);
}
void _setDebugPath(const char *loggingPath, const char *callerPath, const char *callFunction)
{
	if (strcmp(callFunction, "int main()") != 0 && strcmp(callFunction, "int main(int, char**)") != 0 && strcmp(callFunction, "int __cdecl main(int,char *[])") != 0 && strcmp(callFunction, "int __cdecl main()") != 0 && strcmp(callFunction, "class JSON &__cdecl locStat::locate(char *,int,int,int,bool)") != 0)
	{
		std::cout << "setDebugPath should only be called in the main function!" << std::endl;
		throw("setDebugPath should only be called in the main function!");
	}

	if (_rootPath == nullptr)
		setRootPath((char *)callerPath);

	if (_debugLoggingPath != nullptr)
	{
		delete[] _debugLoggingPath;
		delete _logStream;
	}

	int pathSize = (int)strnlen_s(loggingPath, 100);
	_debugLoggingPath = new char[pathSize + 1];
	strcpy_s(_debugLoggingPath, pathSize + 1, loggingPath);

	_logStream = new std::ofstream(_debugLoggingPath, std::ofstream::out);
}

// Function to set the function that is called when an error occurs
void _onDebugExit(DebugExitCallback onExit, const char *callerPath, int callerLine, const char *callFunction)
{
	_onExit = onExit;
}

// Functions used to generate debug logs

/// <summary>
/// Get the time of calling the logging function and return it in string form
/// </summary>
/// <param name="time">The character array used to store the string</param>
/// <returns>The length of the returned string</returns>
int getCallingTime(char **time)
{
	std::time_t end_time = std::time(nullptr);

	*time = new char[100];

	return std::strftime(*time, 100, "%c", std::localtime(&end_time));
}

/// <summary>
/// Find the difference in paths between the path of int main and the path of the current file
/// </summary>
/// <param name="callerPath">The path of the current file</param>
/// <returns>Character array containing a new string with path relative to int main</returns>
char *pathFromMain(char *callerPath)
{
	int length = (int)strlen(callerPath);
	char **pathArray = getPath(callerPath, length);

	// Find the depth of the path where the paths still match
	int matching = -1;
	for (int p = 0; p < _rootPathDepth - 1; p++)
	{
		if (strcmp(pathArray[p], _rootPath[p]) == 0)
			matching++;
		else
			break;
	}

	char *output;

	// If the paths dont match at all, it is probably on a different drive
	// therefore return the whole string
	if (matching == -1)
	{
		// Full string
		output = new char[strlen(callerPath)];
		strcpy(output, callerPath);
	}
	// If the paths match up until a certain depth, print '../' in front of
	// the string until the same depth is reached as int main
	else if (matching < _rootPathDepth - 2)
	{
		// Add ../
		// Amount of ../ necessary
		int dotdotslash = _rootPathDepth - 1 - (matching + 1);
		int pathlength = dotdotslash * 3;

		// Get the length of the full string
		for (int q = matching + 1; q < length; q++)
		{
			pathlength += (int)strlen(pathArray[q]);
			if (q < length - 1)
				pathlength++;
		}
		output = new char[pathlength + 1];
		output[pathlength] = '\0';

		// Hold a temporary pointer to copy all files/folders into the string
		char *o = output;

		// Copy the necessary '../'
		for (int d = 0; d < dotdotslash; d++)
		{
			strcpy(o, "../");
			o += 3;
		}
		// Copy all files/folders
		for (int q = matching + 1; q < length; q++)
		{
			strcpy(o, pathArray[q]);
			o += (int)strlen(pathArray[q]);
			if (q < length - 1)
				o[0] = '/';
			o++;
		}
	}
	// If the paths match until the same depth as the int main file,
	// return the string with './' appended
	else if (matching == _rootPathDepth - 2)
	{
		// Add ./
		// Get the length of the total string
		int pathlength = 2;
		for (int q = matching + 1; q < length; q++)
		{
			pathlength += (int)strlen(pathArray[q]);
			if (q < length - 1)
				pathlength++;
		}
		output = new char[pathlength + 1];
		output[pathlength] = '\0';

		// Hold a temporary pointer to copy all files/folders into the string
		char *o = output;

		strcpy(o, "./");
		o += 2;
		// Copy all files/folders
		for (int q = matching + 1; q < length; q++)
		{
			strcpy(o, pathArray[q]);
			o += (int)strlen(pathArray[q]);
			if (q < length - 1)
				o[0] = '/';
			o++;
		}
	}
	else
	{
		output = new char;
		*output = '\0';
	}

	return output;
}

/// <summary>
/// Function that creates a string in format: [datetime] typeOfMessage In 'function', filename:linenumber:,
/// </summary>
/// <param name="callerPath">Path to current file</param>
/// <param name="prefix">The type of the message, e.g. NOTE, WARN, ERROR</param>
/// <returns>String</returns>
char *debugPrefix(const char *callerPath, char *prefix, char *callFunction, int callerLine)
{
	char *time;
	int t_length = getCallingTime(&time);

	char *pathToPrint = pathFromMain((char *)callerPath);

	char *prefix_s = new char[15 + strlen(pathToPrint) + t_length + 1 + strlen(callFunction) + 7];

	sprintf(prefix_s, "[%s] %s In '%s', %s:%i:", time, prefix, callFunction, pathToPrint, callerLine);

	delete[] time;
	delete[] pathToPrint;

	return prefix_s;
}

void _debugNote(char *log, const char *callerPath, int callerLine, const char *callFunction)
{
	if (!_debugLogging)
		return;
	char *prefix = debugPrefix(callerPath, (char *)"NOTE", (char *)callFunction, callerLine);

	char *debugText = new char[2 + strlen(prefix) + strlen(log)];

	sprintf(debugText, "%s %s", prefix, log);
	delete[] prefix;

	// Do stuff with debugText
	std::cout << debugText << std::endl;
	if (_logStream != nullptr)
		*_logStream << debugText << std::endl;

	delete[] debugText;
}
void _debugNote(const char *log, const char *callerPath, int callerLine, const char *callFunction)
{
	if (!_debugLogging)
		return;
	char *prefix = debugPrefix(callerPath, (char *)"NOTE", (char *)callFunction, callerLine);

	char *debugText = new char[2 + strlen(prefix) + strlen(log)];

	sprintf(debugText, "%s %s", prefix, log);
	delete[] prefix;

	// Do stuff with debugText
	std::cout << debugText << std::endl;
	if (_logStream != nullptr)
		*_logStream << debugText << std::endl;

	delete[] debugText;
}

void _debugWarn(char *log, const char *callerPath, int callerLine, const char *callFunction)
{
	if (!_debugLogging)
		return;
	char *prefix = debugPrefix(callerPath, (char *)"WARN", (char *)callFunction, callerLine);

	char *debugText = new char[2 + strlen(prefix) + strlen(log)];

	sprintf(debugText, "%s %s", prefix, log);
	delete[] prefix;

	// Do stuff with debugText
	std::cout << debugText << std::endl;
	if (_logStream != nullptr)
		*_logStream << debugText << std::endl;

	delete[] debugText;
}
void _debugWarn(const char *log, const char *callerPath, int callerLine, const char *callFunction)
{
	if (!_debugLogging)
		return;
	char *prefix = debugPrefix(callerPath, (char *)"WARN", (char *)callFunction, callerLine);

	char *debugText = new char[2 + strlen(prefix) + strlen(log)];

	sprintf(debugText, "%s %s", prefix, log);
	delete[] prefix;

	// Do stuff with debugText
	std::cout << debugText << std::endl;
	if (_logStream != nullptr)
		*_logStream << debugText << std::endl;

	delete[] debugText;
}

void _debugError(char *log, int exitCode, const char *callerPath, int callerLine, const char *callFunction)
{
	char *prefix = debugPrefix(callerPath, (char *)"ERROR", (char *)callFunction, callerLine);

	char *debugText = new char[2 + strlen(prefix) + strlen(log)];

	sprintf(debugText, "%s %s", prefix, log);
	delete[] prefix;

	// Do stuff with debugText
	std::cout << debugText << std::endl;
	if (_logStream != nullptr)
		*_logStream << debugText << std::endl;

	delete[] debugText;

	if (!_DEBUGH__DEBUG_BUILD)
	{
		if (_onExit == nullptr)
			exit(exitCode);
		else
			_onExit(exitCode);
	}
}
void _debugError(const char *log, int exitCode, const char *callerPath, int callerLine, const char *callFunction)
{
	char *prefix = debugPrefix(callerPath, (char *)"ERROR", (char *)callFunction, callerLine);

	char *debugText = new char[2 + strlen(prefix) + strlen(log)];

	sprintf(debugText, "%s %s", prefix, log);
	delete[] prefix;

	// Do stuff with debugText
	std::cout << debugText << std::endl;
	if (_logStream != nullptr)
		*_logStream << debugText << std::endl;

	delete[] debugText;

	if (!_DEBUGH__DEBUG_BUILD)
	{
		if (_onExit == nullptr)
			exit(exitCode);
		else
			_onExit(exitCode);
	}
}

void _debugSaveImage(cv::Mat &image, const char *filename, const char *callerPath, int callerLine, const char *callFunction)
{
	if (!_debugLogging)
		return;
	system("mkdir debugImages >nul 2>nul");
	cv::imwrite(std::string("./debugImages/") + filename, image);

	std::string note = "Image saved as: ";
	note += filename;

	_debugNote(note.c_str(), callerPath, callerLine, callFunction);
}

void _debugSaveImage(cv::Mat &image, char *filename, const char *callerPath, int callerLine, const char *callFunction)
{
	if (!_debugLogging)
		return;
	system("mkdir debugImages >nul 2>nul");
	cv::imwrite(std::string("./debugImages/") + filename, image);

	std::string note = "Image saved as: ";
	note += filename;

	_debugNote(note.c_str(), callerPath, callerLine, callFunction);
}