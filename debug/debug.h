#pragma once
#include <iostream>
#include <fstream>
#include <chrono>
#include <ctime>
#include <opencv2/imgcodecs.hpp>

#ifndef _DEBUG
#define _DEBUGH__DEBUG_BUILD 0
#else
#define _DEBUGH__DEBUG_BUILD 1
#endif

typedef void (*DebugExitCallback)(int exitCode);

void _setDebugLogging(bool loggingState, const char* callerPath, const char* callFunction);

void _setDebugPath(char* loggingPath, const char* callerPath, const char* callFunction);
void _setDebugPath(const char* loggingPath, const char* callerPath, const char* callFunction);

void _onDebugExit(DebugExitCallback onExit, const char* callerPath, int callerLine, const char* callFunction);

// Functions for logging events
void _debugNote(char* log, const char* callerPath, int callerLine, const char* callFunction);
void _debugNote(const char* log, const char* callerPath, int callerLine, const char* callFunction);

void _debugWarn(char* log, const char* callerPath, int callerLine, const char* callFunction);
void _debugWarn(const char* log, const char* callerPath, int callerLine, const char* callFunction);

void _debugError(char* log, int exitCode, const char* callerPath, int callerLine, const char* callFunction);
void _debugError(const char* log, int exitCode, const char* callerPath, int callerLine, const char* callFunction);

void _debugSaveImage(cv::Mat& image, const char* filename, char* callerPath, int callerLine, const char* callFunction);
void _debugSaveImage(cv::Mat& image, const char* filename, const char* callerPath, int callerLine, const char* callFunction);

// Macros to eliminate the need for the callerPath, callerLine, and callerFunction input arguments
// When _MSC_VER is defined, a microsoft compiler is used, therefore, use __FUNCSIG__, since
// __PRETTY_FUNCTION__ is not defined in VC++
#ifdef _MSC_VER
#define setDebugLogging(loggingState) _setDebugLogging(loggingState, __FILE__, __FUNCSIG__);
#define setDebugPath(loggingPath) _setDebugPath(loggingPath, __FILE__, __FUNCSIG__);
#define onDebugExit(onexit) _onDebugExit(onexit, __FILE__,__LINE__,__FUNCSIG__);

#define debugNote(log) _debugNote(log, __FILE__, __LINE__, __FUNCSIG__);
#define debugWarn(log) _debugWarn(log, __FILE__, __LINE__, __FUNCSIG__);
#define debugError(log, exitCode) _debugError(log, exitCode, __FILE__, __LINE__, __FUNCSIG__);
#define debugSaveImage(image, filename) _debugSaveImage(image, filename, __FILE__, __LINE__, __FUNCSIG__);
#else
#define setDebugLogging(loggingState) _setDebugLogging(loggingState, __FILE__, __PRETTY_FUNCTION__);
#define setDebugPath(loggingPath) _setDebugPath(loggingPath, __FILE__, __PRETTY_FUNCTION__);
#define onDebugExit(onexit) _onDebugExit(onexit, __FILE__,__LINE__,__PRETTY_FUNCTION__);

#define debugNote(log) _debugNote(log, __FILE__, __LINE__, __PRETTY_FUNCTION__);
#define debugWarn(log) _debugWarn(log, __FILE__, __LINE__, __PRETTY_FUNCTION__);
#define debugError(log, exitCode) _debugError(log, exitCode, __FILE__, __LINE__, __PRETTY_FUNCTION__);
#define debugSaveImage(image, filename) _debugSaveImage(image, filename, __FILE__, __LINE__, __PRETTY_FUNCTION__);
#endif