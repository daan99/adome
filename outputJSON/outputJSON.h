#pragma once

#include <vector>
#include <string>
#include "../colordetection/gridDetection.h"

// Base JSON class, create child of class for new datatypes
class JSON {
private:
	std::vector<JSON*> entries;
	std::vector<char*> keys;
public:
	JSON() {
		;
	}

	void newEntry(char* key, JSON* entry) {
		entries.push_back(entry);
		keys.push_back(key);
	}

	void space(std::string& jsonString, unsigned int depth) {
		std::string tab(1, '\t');
		for (unsigned int i = 0; i < depth; i++) jsonString += tab;
	}

	virtual void toString(std::string& jsonString, unsigned int depth = 0) {
		std::string comma(1, ',');
		std::string newline(1, '\n');
		jsonString += std::string(1, '{') + newline;
		depth++;

		for (int k = 0; k < keys.size(); k++) {
			space(jsonString, depth);
			jsonString += std::string(1, '"') + keys[k] + std::string("\": ");
			entries[k]->toString(jsonString, depth + 1);
			if (k + 1 < keys.size()) jsonString += comma + newline;
		}

		space(jsonString, depth - 1);
		jsonString += std::string(1, '}') + newline;
	}

	void printJson(unsigned int depth = 0) {
		std::string jsonstring;
		toString(jsonstring,depth);
		std::cout << jsonstring;
	}
};

class JSONArray : public JSON {
private:
	std::vector<JSON*> entries;
public:
	JSONArray() {
		;
	}

	void push(JSON* entry) {
		entries.push_back(entry);
	}

	void toString(std::string& jsonString, unsigned int depth = 0) {
		std::string comma(1, ',');
		std::string newline(1, '\n');
		jsonString += std::string(1, '[') + newline;
		depth++;

		for (int k = 0; k < entries.size(); k++) {
			space(jsonString, depth+1);
			entries[k]->toString(jsonString, depth+1);
			if (k + 1 < entries.size()) jsonString += comma + newline;
			else jsonString += newline;
		}

		space(jsonString, depth - 1);
		jsonString += std::string(1, ']') + newline;
	}
};

class JSONLed : public JSON {
private:
	int led;
	Coords* coords;
public:
	JSONLed(int led, Coords* coords) : led{ led }, coords{ coords } {
		;
	}

	void toString(std::string& jsonString, unsigned int depth) {
		std::string comma(1, ',');
		std::string newline(1, '\n');
		jsonString += std::string(1, '{') + newline;
		depth++;
		space(jsonString, depth);
		jsonString += std::string("\"ledId\": ") + std::to_string(led) + comma + newline;
		space(jsonString, depth);
		if (coords) {
			jsonString += std::string("\"found\": true,") + newline;
			space(jsonString, depth);
			jsonString += std::string("\"theta\": ") + std::to_string(coords->theta) + comma + newline;
			space(jsonString, depth);
			jsonString += std::string("\"phi\": ") + std::to_string(coords->phi) + newline;
		}
		else {
			jsonString += std::string("\"found\": false,") + newline;
			space(jsonString, depth);
			jsonString += std::string("\"theta\": 0") + comma + newline;
			space(jsonString, depth);
			jsonString += std::string("\"phi\": 0") + newline;
		}
		space(jsonString, depth - 1);
		jsonString += std::string(1, '}');
	}
};

class JSONAntenna : public JSON {
private:
	int id;
	std::vector<Coords*> coords;
	
public:
	JSONAntenna(int id) : id{ id } {
		;
	}

	void push(Coords* coord) {
		coords.push_back(coord);
	}

	void toString(std::string& jsonString, unsigned int depth) {
		JSONArray arr;
		std::string comma(1, ',');
		std::string newline(1, '\n');

		for (int c = 0; c < (int)coords.size(); c++) {
			JSONLed* tempLed = new JSONLed(c + 1, coords[c]);
			arr.push(tempLed);
		}

		jsonString += std::string(1, '{') + newline;
		depth++;
		space(jsonString, depth);
		jsonString += std::string("\"id\": ") + std::to_string(id) + comma + newline;
		space(jsonString, depth);
		jsonString += std::string("\"leds\": ");
		arr.toString(jsonString, depth);
		space(jsonString, depth - 1);
		jsonString += std::string(1, '}');
	}
};