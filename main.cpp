#define CV_WARN(message)

// #include <Windows.h>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <math.h>

#include <opencv2/core/ocl.hpp>

#include "colordetection/colorDetection.h"
#include "colordetection/gridDetection.h"
#include "colordetection/angleMap.h"
#include "image/editImage.h"
#include "outputJSON/outputJSON.h"

#include "hardwareControls/ledControl.h"

#include "debug/debug.h"

#define RGB(r, g, b) Scalar(b, g, r)

// ERROR CODES
// - -1: undefined error
// - 1: Syntax error
// - 2: Serial error
// - 3: LED control error
// - 4: Failing to open data files
// - 5: Camera is not opened

using namespace cv;

// Start video feed
VideoCapture cap(0, CAP_DSHOW);

void onExit()
{
    cap.release();
}

bool lowExp = false;

void switchExp(VideoCapture &cap)
{
    if (lowExp)
    {
        cap.set(CAP_PROP_EXPOSURE, -4);
    }
    else
    {
        cap.set(CAP_PROP_EXPOSURE, -7);
    }
    lowExp = !lowExp;
}

void takePictures(VideoCapture &capture, Mat &frame_on, Mat &frame_off, Led &led)
{
    Mat img;

    led.setPower(true);
    capture >> img;
    frame_off = cropToPercentage(img, 0.80);

    led.setPower(false);
    capture >> img;
    frame_on = cropToPercentage(img, 0.80);
}

Point findLed(Led &led, int antennaId, int ledId, int trials = 1)
{
    Mat frame_off, frame_on;

    Point LED(-1, -1);

    int trial = 0;

    for (trial = 0; trial < trials && LED == Point(-1, -1); trial++)
    {
        takePictures(cap, frame_on, frame_off, led);
        LED = locateLED(frame_on, frame_off, 3, 10, 5);
    }

    std::string fileNameBase = "_ant";
    fileNameBase += std::to_string(antennaId) + std::string("_led_") + std::to_string(ledId) + std::string("_trial_") + std::to_string(trial - 1);
    std::string fileNameOn = "frame_on";
    fileNameOn += fileNameBase + std::string(".jpg");
    std::string fileNameOff = "frame_off";
    fileNameOff += fileNameBase + std::string(".jpg");

    drawMarker(frame_off, LED, Scalar(255, 0, 255), MARKER_CROSS, 100, 1);
    debugSaveImage(frame_on, fileNameOn.c_str());
    debugSaveImage(frame_off, fileNameOff.c_str());

    return LED;
}

// INPUT ARGUMENTS: [2]
int no_args = 4;
char INPUTARGS[] = "\t- COM port of ADome connection\n\t- Number of antennas in the ADome\n\t- Number of leds per antenna\n\t- Number of trials";

int handleOCVError(int status, const char *func_name,
                   const char *err_msg, const char *file_name,
                   int line, void *userdata)
{
    std::cout << "Undefined error in: " << file_name << ", on line: " << line << std::endl;
    std::cout << err_msg << std::endl;

    exit(-1);

    return 0;
}

int main(int argc, char *argv[])
{
    setDebugLogging(true);
    setDebugPath("./latestLocalizationRun.log");

    cv::redirectError(handleOCVError);
    debugNote("DEBUG MODE ENABLED");
    atexit(onExit);

    if (!cap.isOpened())
    {
        debugError("Camera has not been opened!", 5);
    }

    if (argc != no_args + 1)
    {
        std::string synError = "Syntax error: please ensure that the correct input arguments have been provided: \n";
        synError += std::string(INPUTARGS);
        debugError(synError.c_str(), 1);
    }

    char *COM = argv[1];
    int numberOfAntennas;
    (void)sscanf(argv[2], "%i", &numberOfAntennas);
    int numberOfLeds;
    (void)sscanf(argv[3], "%i", &numberOfLeds);
    int trials;
    (void)sscanf(argv[4], "%i", &trials);
    if (trials < 1)
    {
        debugError("Number of trials should be at least equal to 1", 1);
    }

    JSON json;
    JSONArray *arr = new JSONArray;
    json.newEntry((char *)"antennas", arr);

    cap.set(CAP_PROP_FRAME_HEIGHT, 1080);
    cap.set(CAP_PROP_FRAME_WIDTH, 1920);
    cap.set(CAP_PROP_AUTO_EXPOSURE, 0.25);

    // switchExp(cap);
    cap.set(CAP_PROP_EXPOSURE, -12);

    Led led(COM, 0, false);
    led.setColor(Scalar(0, 0, 255));

    AngleMap angleMap("AngleMap2");

    // TODO implement looping through all leds

    for (int a = 0; a < numberOfAntennas; a++)
    {
        JSONAntenna *ant = new JSONAntenna(a);

        for (int l = 0; l < numberOfLeds; l++)
        {
            Coords *ledCoords = NULL;

            Point ledCart = findLed(led, a, l, trials);
            if (ledCart != Point(-1, -1))
                ledCoords = new Coords(angleMap.FindCoordsCombi(ledCart.x, ledCart.y));
            std::cout << "(" << ledCoords->phi << ", " << ledCoords->theta << ")" << std::endl;

            ant->push(ledCoords);
        }

        arr->push(ant);
    }

    json.printJson();

    cap.release();

    return 0;
}