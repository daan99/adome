#define CV_WARN(message)

// #include <Windows.h>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <math.h>

// #include <opencv2\core\ocl.hpp>

#include "../colordetection/colorDetection.h"
#include "../colordetection/gridDetection.h"
#include "../colordetection/angleMap.h"

#include "../image/editImage.h"
#include "../outputJSON/outputJSON.h"

// #include "hardwareControls/ledControl.h"

#include "debug/debug.h"

#define RGB(r, g, b) Scalar(b, g, r)

// ERROR CODES
// - -1: undefined error
// - 1: Syntax error
// - 2: Serial error
// - 3: LED control error
// - 4: Failing to open data files
// - 5: Camera is not opened

using namespace cv;

// Start video feed
VideoCapture cap(0, CAP_DSHOW);

void onExit()
{
    cap.release();
}

bool lowExp = false;

void switchExp(VideoCapture &cap)
{
    if (lowExp)
    {
        cap.set(CAP_PROP_EXPOSURE, -4);
    }
    else
    {
        cap.set(CAP_PROP_EXPOSURE, -7);
    }
    lowExp = !lowExp;
}

// INPUT ARGUMENTS: [2]
int no_args = 4;
char INPUTARGS[] = "\t- COM port of ADome connection\n\t- Number of antennas in the ADome\n\t- Number of leds per antenna\n\t- Number of trials";

int main(int argc, char *argv[])
{

    atexit(onExit);

    if (argc != no_args + 1)
    {
        std::string synError = "Syntax error: please ensure that the correct input arguments have been provided: \n";
        synError += std::string(INPUTARGS);
    }

    char *COM = argv[1];
    int numberOfAntennas;
    (void)sscanf(argv[2], "%i", &numberOfAntennas);
    int numberOfLeds;
    (void)sscanf(argv[3], "%i", &numberOfLeds);
    int trials;
    (void)sscanf(argv[4], "%i", &trials);

    cap.set(CAP_PROP_FRAME_HEIGHT, 1080);
    cap.set(CAP_PROP_FRAME_WIDTH, 1920);
    cap.set(CAP_PROP_AUTO_EXPOSURE, 0.25);

    // switchExp(cap);
    cap.set(CAP_PROP_EXPOSURE, -12);

    AngleMap angleMap("AngleMap2");

    AngleMap NewMap = AngleMap("AngleMap0506");
    std::string url = samples::findFile("pictures/LINKSOM_FILTER2/LED_4_off.jpg");
    Mat img = imread(url, IMREAD_ANYCOLOR);

    Mat frame;
    Mat frame_org = cropToPercentage(img, 0.80);

    std::string WindowName = "Move marker accros screen";
    namedWindow(WindowName, WINDOW_KEEPRATIO);

    int PHITESTX = 400;
    int PHITESTY = 400;

    int XOLD = 0;
    int YOLD = 0;

    while (true)
    {

        { // Draw Lines on the screen
            if (XOLD != PHITESTX || YOLD != PHITESTY)
            {

                frame_org.copyTo(frame);

                NewMap.DrawCenter(frame);
                NewMap.DrawReferencePoints(frame);
                double e = 1;
                int div = 10;
                int kleurIndexDingetje = 0;

                NewMap.FindCoordsExtraP(PHITESTX, PHITESTY, frame);

                Coords tmpCoords = NewMap.FindCoordsCombi(PHITESTX, PHITESTY);

                std::stringstream ss;
                ss << "[" << tmpCoords.theta << ", " << tmpCoords.phi << "]";
                putText(frame, ss.str(), Point(PHITESTX, PHITESTY), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.0, cv::Scalar(255, 255, 255), 1, cv::LINE_AA);

                imshow(WindowName, frame);
                XOLD = PHITESTX;
                YOLD = PHITESTY;
            }
        }

        { // Keyboard actions
            char key = (char)waitKey(30);
            if (key == 'x' || key == 'X')
            {
                break;
            }
            else if (key == 'T')
            {
                PHITESTY += 10;
            }
            else if (key == 'R')
            {
                PHITESTY -= 10;
            }
            else if (key == 'S')
            {
                PHITESTX += 10;
            }
            else if (key == 'Q')
            {
                PHITESTX -= 10;
            }
        }
    }

    return 0;
}