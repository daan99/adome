#define CV_WARN(message)

// #include <Windows.h>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <math.h>

// #include <opencv2\core\ocl.hpp>

#include "../colordetection/colorDetection.h"
#include "../colordetection/gridDetection.h"
#include "../colordetection/angleMap.h"
#include "../image/editImage.h"
#include "../outputJSON/outputJSON.h"

// #include "hardwareControls/ledControl.h"

#include "debug/debug.h"

#define RGB(r, g, b) Scalar(b, g, r)

// ERROR CODES
// - -1: undefined error
// - 1: Syntax error
// - 2: Serial error
// - 3: LED control error
// - 4: Failing to open data files
// - 5: Camera is not opened

using namespace cv;

// Start video feed
VideoCapture cap(0, CAP_DSHOW);

void onExit()
{
    cap.release();
}

bool lowExp = false;

void switchExp(VideoCapture &cap)
{
    if (lowExp)
    {
        cap.set(CAP_PROP_EXPOSURE, -4);
    }
    else
    {
        cap.set(CAP_PROP_EXPOSURE, -7);
    }
    lowExp = !lowExp;
}

// INPUT ARGUMENTS: [2]
int no_args = 4;
char INPUTARGS[] = "\t- COM port of ADome connection\n\t- Number of antennas in the ADome\n\t- Number of leds per antenna\n\t- Number of trials";

int main(int argc, char *argv[])
{

    atexit(onExit);

    if (argc != no_args + 1)
    {
        std::string synError = "Syntax error: please ensure that the correct input arguments have been provided: \n";
        synError += std::string(INPUTARGS);
    }

    double e = 0.5;
    int div = 5;

    if (argc == 2)
    {
        (void)sscanf(argv[1], "%i", &div);
    }
    else if (argc == 3)
    {
        (void)sscanf(argv[2], "%f", &e);
    }
    else if (argc > 3)
    {
        std::cout << "ERROR: TOO MANY IMPUT ARGUMENTS GIVEN! \n";
        return 1;
    }

    std::cout << "DIV: " << div << std::endl;
    AngleMap NewMap = AngleMap("../anglemaps/AngleMap7");
    std::string url = samples::findFile("pictures/LINKSOM_FILTER2/LED_4_off.jpg");
    Mat img = imread(url, IMREAD_ANYCOLOR);

    Mat frame;
    Mat frame_org = cropToPercentage(img, 0.80);

    std::string WindowName = "Whole grid";
    namedWindow(WindowName, WINDOW_KEEPRATIO);

    int PHITESTX = 400;
    int PHITESTY = 400;

    std::cout << "\n\nSTART\n";
    { // Draw Lines on the screen

        std::cout << "DEBUG 1\n";

        frame_org.copyTo(frame);

        std::cout << "DEBUG 2\n";
        NewMap.DrawCenter(frame);
        int kleurIndexDingetje = 0;

        std::cout << "DEBUG 3\n";

        int Blue[3] = {255, 0, 255};
        int Green[3] = {255, 255, 0};
        int Red[3] = {0, 255, 255};

        for (int i = 0; i < frame.cols / div; i++)
        {
            for (int j = 0; j < frame.rows / div; j++)
            {
                Coords tmpCoords = NewMap.FindCoordsCombi(i * div, j * div);

                // Draw phi for every 10 according to AngleMap
                if ((int)tmpCoords.phi % 10 <= e || (int)tmpCoords.phi % 10 >= 10 - e)
                {
                    // std::cout << "PHI: " << tmpCoords.phi << std::endl;
                    // drawMarker(frame, cv::Point(i*div, j*div), cv::Scalar(255,255,255), cv::MARKER_CROSS, 10, 2);
                    int KLEURINDEX = (int)((tmpCoords.phi / 10) + 0.5) % 3;
                    drawMarker(frame, cv::Point(i * div, j * div), cv::Scalar(Blue[KLEURINDEX], Green[KLEURINDEX], Red[KLEURINDEX]), cv::MARKER_CROSS, 5, 2);

                    if (tmpCoords.phi > 360)
                    {
                        drawMarker(frame, cv::Point(i * div, j * div), cv::Scalar(0, 0, 255), cv::MARKER_CROSS, 5, 2);
                    }
                }

                if ((int)tmpCoords.theta % 10 <= e || (int)tmpCoords.theta % 10 >= 10 - e)
                {
                    drawMarker(frame, cv::Point(i * div, j * div), cv::Scalar(255, 255, 255), cv::MARKER_CROSS, 5, 2);
                }
            }
        }
        imshow(WindowName, frame);
    }

    std::cout << "DEBUG 1\n";
    while (true)
    {
        { // Keyboard actions
            char key = (char)waitKey(30);
            if (key == 'x' || key == 'X')
            {
                break;
            }
            else if (key == 'T')
            {
                PHITESTY += 10;
            }
            else if (key == 'R')
            {
                PHITESTY -= 10;
            }
            else if (key == 'S')
            {
                PHITESTX += 10;
            }
            else if (key == 'Q')
            {
                PHITESTX -= 10;
            }
        }
    }

    return 0;
}