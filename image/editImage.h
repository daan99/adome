#pragma once



#include<opencv2/core.hpp>
#include<opencv2/highgui.hpp>
#include<opencv2/imgproc.hpp>
using namespace cv;

#include<iostream>

#define RGB(r,g,b) Scalar(b,g,r)

/// <summary>
/// Crop image to a given percentage,
/// the box to which the image will be cropped can also be drawn on the original image
/// </summary>
/// <param name="src">Source image that will be cropped</param>
/// <param name="percentage">The percentage to which the source image will be cropped</param>
/// <param name="ShowOnOriginal">Flag which determines whether or not the ROI will be displayed on the source image</param>
/// <returns>cv::Mat with the cropped image</returns>
Mat cropToPercentage(Mat& src, double percentage, bool ShowOnOriginal = false);

/// <summary>
/// Draws a crosshair on a given image
/// </summary>
/// <param name="src">Source image that will have a crosshair drawn on it</param>
/// <returns>cv::Mat with the final image</returns>
Mat DrawCrosshair(Mat& src);