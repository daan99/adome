#include "editImage.h"

Mat cropToPercentage(Mat& src, double percentage, bool ShowOnOriginal) {
    
    if (percentage > 1 || percentage <= 0) {
        std::cout << "cropToPercentage: trying to crop to a wrong percentage (" << percentage << ")\n The value 1 will be used as percentage to crop the image\n";
        percentage = 1;
    }


    //Create rectangular ROI to which the source will be cropped
    Rect ROI;
    ROI.x = (uint)(((1 - percentage) / 2) * src.cols); // x coordinate of upper right corner of ROI
    ROI.y = (uint)(((1 - percentage) / 2) * src.rows); // y coordinate of upper right corner of ROI
    ROI.width = (uint)(percentage * src.cols); // width of ROI
    ROI.height = (uint)(percentage * src.rows); // height of ROI

    Mat croppedFrame;
    croppedFrame = src(ROI);

    //Copy for better robustness against different types of matrices
    Mat frame;
    croppedFrame.copyTo(frame);


    //Show the 
    if (ShowOnOriginal)
        rectangle(src, ROI, RGB(255, 0, 0));


    return frame;
}

Mat DrawCrosshair(Mat& src) {
    Mat result;
    src.copyTo(result);

    //Horizontal Line
    int Height = src.rows / 2;

    Point H1(0, Height);
    Point H2(src.cols, Height);
    line(result, H1, H2, RGB(0, 255, 0), 2);

    
    //Vertical Line
    int Width = src.cols / 2;

    Point V1(Width, 0);
    Point V2(Width, src.rows);
    line(result, V1, V2, RGB(0, 255, 0), 2);

    return result;
}