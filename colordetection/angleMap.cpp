#include "angleMap.h"

double DistanceLineToPoint(cv::Point P1, cv::Point P2, cv::Point X)
{
    double distance = abs((P2.x - P1.x) * (P1.y - X.y) - (P1.x - X.x) * (P2.y - P1.y)) / sqrt((P2.x - P1.x) * (P2.x - P1.x) + (P2.y - P1.y) * (P2.y - P1.y));
    return distance;
}

void AngleMap::DrawCenter(cv::Mat frame)
{
    if (CenterIndex == -1)
    {
        for (int i = 0; i < PixAngVector.size(); i++)
        {
            if (PixAngVector[i].center)
            {
                CenterIndex = i;
            }
        }
    }
    cv::drawMarker(frame, cv::Point(PixAngVector[CenterIndex].x, PixAngVector[CenterIndex].y), cv::Scalar(0, 255, 355), cv::MARKER_CROSS, 10, 2);
    cv::putText(frame, "(0, 0)", cv::Point(PixAngVector[CenterIndex].x, PixAngVector[CenterIndex].y), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.0, cv::Scalar(255, 255, 255), 1, cv::LINE_AA);
}

void AngleMap::DrawReferencePoints(cv::Mat frame)
{
    for (int i = 0; i < PixAngVector.size(); i++)
    {
        cv::drawMarker(frame, cv::Point(PixAngVector[i].x, PixAngVector[i].y), cv::Scalar(255, 255, 255), cv::MARKER_CROSS, 10, 2);
    }
}

Coords AngleMap::FindCoordsPlane(int x, int y)
{

    int N = 5;

    std::vector<double> DistanceList;
    std::vector<int> IndexList;
    for (int i = 0; i < N; i++)
    {
        DistanceList.push_back(-1);
        IndexList.push_back(-1);
    }

    /*
        Compare the point with index 'i' to all other nodes to find how it lies with respect to point 'i'
    */
    for (int i = 0; i < PixAngVector.size(); i++)
    {

        double xDifference = PixAngVector[i].x - x;
        double yDifference = PixAngVector[i].y - y;

        double Distance = sqrt(xDifference * xDifference + yDifference * yDifference);

        for (int k = 0; k < N; k++)
        {
            if (Distance < DistanceList[k] || DistanceList[k] == -1)
            {
                for (int l = N - 1; l >= k + 1; l--)
                {
                    DistanceList[l] = DistanceList[l - 1];
                    IndexList[l] = IndexList[l - 1];
                }
                IndexList[k] = i;
                DistanceList[k] = Distance;

                break;
            }
        }
    }

    double theta, phi, R2;
    if (DistanceList[0] < 4)
    {
        theta = PixAngVector[IndexList[0]].theta;
        phi = PixAngVector[IndexList[0]].phi;
        // std::cout << "Point is so close to a calibration dot, plane fit is not done\n";
    }
    else
    {
        // Fit a plane through the N points

        double Sx2, Sxy, Sx, Sy2, Sy, S1, SxzTheta, SxzPhi, SyzTheta, SyzPhi, SzTheta, SzPhi;
        Sx2 = Sxy = Sx = Sy2 = Sy = S1 = SxzTheta = SxzPhi = SyzTheta = SyzPhi = SzTheta = SzPhi = 0;

        /*
        Check whether points are near phi = 0 or phi = 360
        */
        bool justAbove0 = false;
        bool justBelow0 = false;
        for (int i = 0; i < N; i++)
        {
            if (PixAngVector[IndexList[i]].phi < 30)
            {
                justAbove0 = true;
            }
            if (PixAngVector[IndexList[i]].phi > 330)
            {
                justBelow0 = true;
            }
        }
        bool aroundPhi0 = false;
        if (justAbove0 && justBelow0)
        {
            aroundPhi0 = true;
        }

        /*
        Take all the sums necessary for the plane approximation
        */

        // std::cout << "PixAngs Used: \n";
        for (int i = 0; i < N; i++)
        {
            Sx2 += PixAngVector[IndexList[i]].x * PixAngVector[IndexList[i]].x;
            Sxy += PixAngVector[IndexList[i]].x * PixAngVector[IndexList[i]].y;
            Sx += PixAngVector[IndexList[i]].x;
            Sy2 += PixAngVector[IndexList[i]].y * PixAngVector[IndexList[i]].y;
            Sy += PixAngVector[IndexList[i]].y;
            S1 += 1;

            SxzTheta += PixAngVector[IndexList[i]].x * PixAngVector[IndexList[i]].theta;
            SyzTheta += PixAngVector[IndexList[i]].y * PixAngVector[IndexList[i]].theta;
            SzTheta += PixAngVector[IndexList[i]].theta;

            if (aroundPhi0 && PixAngVector[IndexList[i]].phi < 180)
            {
                SxzPhi += PixAngVector[IndexList[i]].x * (PixAngVector[IndexList[i]].phi + 360);
                SyzPhi += PixAngVector[IndexList[i]].y * (PixAngVector[IndexList[i]].phi + 360);
                SzPhi += PixAngVector[IndexList[i]].phi + 360;
            }
            else
            {
                SxzPhi += PixAngVector[IndexList[i]].x * PixAngVector[IndexList[i]].phi;
                SyzPhi += PixAngVector[IndexList[i]].y * PixAngVector[IndexList[i]].phi;
                SzPhi += PixAngVector[IndexList[i]].phi;
            }
        }

        double Mat1[3][3];

        Mat1[0][0] = Sx2;
        Mat1[0][1] = Sxy;
        Mat1[0][2] = Sx;

        Mat1[1][0] = Sxy;
        Mat1[1][1] = Sy2;
        Mat1[1][2] = Sy;

        Mat1[2][0] = Sx;
        Mat1[2][1] = Sy;
        Mat1[2][2] = S1;

        /*
            Do matrix inversion
        */
        double determinant = Mat1[0][0] * (Mat1[1][1] * Mat1[2][2] - Mat1[2][1] * Mat1[1][2]) + Mat1[0][1] * (Mat1[1][2] * Mat1[2][0] - Mat1[1][0] * Mat1[2][2]) + Mat1[0][2] * (Mat1[1][0] * Mat1[2][1] - Mat1[2][0] * Mat1[1][1]);

        double InvMat1[3][3];
        InvMat1[0][0] = (Mat1[1][1] * Mat1[2][2] - Mat1[1][2] * Mat1[2][1]) / determinant;
        InvMat1[0][1] = (Mat1[0][2] * Mat1[2][1] - Mat1[0][1] * Mat1[2][2]) / determinant;
        InvMat1[0][2] = (Mat1[0][1] * Mat1[1][2] - Mat1[0][2] * Mat1[1][1]) / determinant;

        InvMat1[1][0] = (Mat1[1][2] * Mat1[2][0] - Mat1[1][0] * Mat1[2][2]) / determinant;
        InvMat1[1][1] = (Mat1[0][0] * Mat1[2][2] - Mat1[0][2] * Mat1[2][0]) / determinant;
        InvMat1[1][2] = (Mat1[0][2] * Mat1[1][0] - Mat1[0][0] * Mat1[1][2]) / determinant;

        InvMat1[2][0] = (Mat1[1][0] * Mat1[2][1] - Mat1[1][1] * Mat1[2][0]) / determinant;
        InvMat1[2][1] = (Mat1[0][1] * Mat1[2][0] - Mat1[0][0] * Mat1[2][1]) / determinant;
        InvMat1[2][2] = (Mat1[0][0] * Mat1[1][1] - Mat1[0][1] * Mat1[1][0]) / determinant;

        /*
        Calculate A, B and C for z = AX + BY + C for both theta and phi
        */
        double Atheta = InvMat1[0][0] * SxzTheta + InvMat1[0][1] * SyzTheta + InvMat1[0][2] * SzTheta;
        double Btheta = InvMat1[1][0] * SxzTheta + InvMat1[1][1] * SyzTheta + InvMat1[1][2] * SzTheta;
        double Ctheta = InvMat1[2][0] * SxzTheta + InvMat1[2][1] * SyzTheta + InvMat1[2][2] * SzTheta;

        double Aphi = InvMat1[0][0] * SxzPhi + InvMat1[0][1] * SyzPhi + InvMat1[0][2] * SzPhi;
        double Bphi = InvMat1[1][0] * SxzPhi + InvMat1[1][1] * SyzPhi + InvMat1[1][2] * SzPhi;
        double Cphi = InvMat1[2][0] * SxzPhi + InvMat1[2][1] * SyzPhi + InvMat1[2][2] * SzPhi;

        // Calc the quality the accuracy of the fit
        double R2Den = 0;
        double R2Num = 0;
        for (int i = 0; i < N; i++)
        {
            double zi = (PixAngVector[IndexList[i]].theta) - (Atheta * PixAngVector[IndexList[i]].x + Btheta * PixAngVector[IndexList[i]].y + Ctheta);
            R2Den += zi * zi;

            zi = (PixAngVector[IndexList[i]].theta) - (1 / N) * SzTheta;
            R2Num += zi * zi;
        }
        R2 = 1 - (R2Den / R2Num);

        // Fill in the x and y in the plane equations to find theta and phi
        theta = Atheta * x + Btheta * y + Ctheta;
        phi = Aphi * x + Bphi * y + Cphi;
    }

    if (phi >= 360)
    {
        phi -= 360;
    }

    return Coords(theta, phi);
}

Coords AngleMap::FindCoordsPlane(Point point)
{
    return this->FindCoordsPlane(point.x, point.y);
}

Coords AngleMap::FindCoordsExtraP(int x, int y, cv::Mat frame, bool drawLine)
{
    PixAng Center = PixAngVector[CenterIndex];

    // Find the N closest points to the Line through the center and x,y;
    const int N = 8;

    // Declare the Index and Distance List and fill them with -1s
    std::vector<double> DistanceList;
    std::vector<int> IndexList;
    for (int i = 0; i < N; i++)
    {
        DistanceList.push_back(-1);
        IndexList.push_back(-1);
    }

    cv::Point CenterPoint = cv::Point(Center.x, Center.y);
    cv::Point RefPoint = cv::Point(x, y);

    // Define Quadrant of RefPoint

    double RefAngle = atan2((y - CenterPoint.y), (x - CenterPoint.x));

    for (int i = 0; i < PixAngVector.size(); i++)
    {

        PixAng LinePixAng = PixAngVector[i];
        cv::Point LinePoint = cv::Point(LinePixAng.x, LinePixAng.y);

        double LineAngle = atan2((LinePoint.y - CenterPoint.y), (LinePoint.x - CenterPoint.x));
        // If the reference point is not within 10 degrees skip this point
        if (abs(RefAngle - LineAngle) > M_PI / 18)
            continue;

        double Distance = DistanceLineToPoint(CenterPoint, LinePoint, RefPoint);

        for (int k = 0; k < N; k++)
        {
            if (Distance < DistanceList[k] || DistanceList[k] == -1)
            {
                for (int l = N - 1; l >= k + 1; l--)
                {
                    DistanceList[l] = DistanceList[l - 1];
                    IndexList[l] = IndexList[l - 1];
                }
                IndexList[k] = i;
                DistanceList[k] = Distance;

                break;
            }
        }
    }

    // Draw The Lines and the reference point
    for (int i = 0; i < N; i++)
    {
        if (IndexList[i] == -1)
        {
            continue;
        }

        // Line y = ax + b
        cv::Point LinePoint = cv::Point(PixAngVector[IndexList[i]].x, PixAngVector[IndexList[i]].y);

        cv::drawMarker(frame, LinePoint, cv::Scalar(255, 0, 255), cv::MARKER_CROSS, 10, 2);
        cv::putText(frame, std::to_string(i), LinePoint, cv::FONT_HERSHEY_COMPLEX_SMALL, 1.0, cv::Scalar(255, 0, 255), 1, cv::LINE_AA);

        cv::line(frame, CenterPoint, LinePoint, cv::Scalar(255, 255, 255));
    }

    cv::drawMarker(frame, cv::Point(x, y), cv::Scalar(255, 255, 0), cv::MARKER_CROSS, 10, 2);

    double phi = 0;
    double WeightSum = 0;
    bool NearEdge = false;

    for (int i = 0; i < N; i++)
    {
        if (IndexList[i] != -1)
        {
            double curPhi = PixAngVector[IndexList[i]].phi;

            if (curPhi < 10 || curPhi > 350)
            {
                NearEdge = true;
            }

            if (NearEdge && curPhi < 10)
            {
                curPhi += 180;
            }

            double distance = DistanceList[i];
            phi += curPhi * (1 / distance);
            WeightSum += (1 / distance);

            if (distance < 2)
            {
                return Coords(0, PixAngVector[IndexList[i]].phi);
            }
        }
    }
    phi /= WeightSum;

    Coords X = Coords(0, phi);

    return X;
}

Coords AngleMap::FindCoordsExtraP(int x, int y)
{
    PixAng Center = PixAngVector[CenterIndex];

    // Find the N closest points to the Line through the center and x,y;
    const int N = 8;

    // Declare the Index and Distance List and fill them with -1s
    std::vector<double> DistanceList;
    std::vector<int> IndexList;
    for (int i = 0; i < N; i++)
    {
        DistanceList.push_back(-1);
        IndexList.push_back(-1);
    }

    cv::Point CenterPoint = cv::Point(Center.x, Center.y);
    cv::Point RefPoint = cv::Point(x, y);

    // Define Quadrant of RefPoint

    double RefAngle = atan2((y - CenterPoint.y), (x - CenterPoint.x));

    for (int i = 0; i < PixAngVector.size(); i++)
    {

        PixAng LinePixAng = PixAngVector[i];
        cv::Point LinePoint = cv::Point(LinePixAng.x, LinePixAng.y);

        double LineAngle = atan2((LinePoint.y - CenterPoint.y), (LinePoint.x - CenterPoint.x));
        // If the reference point is not within 10 degrees skip this point
        if (abs(RefAngle - LineAngle) > M_PI / 18)
            continue;

        double Distance = DistanceLineToPoint(CenterPoint, LinePoint, RefPoint);

        for (int k = 0; k < N; k++)
        {
            if (Distance < DistanceList[k] || DistanceList[k] == -1)
            {
                for (int l = N - 1; l >= k + 1; l--)
                {
                    DistanceList[l] = DistanceList[l - 1];
                    IndexList[l] = IndexList[l - 1];
                }
                IndexList[k] = i;
                DistanceList[k] = Distance;

                break;
            }
        }
    }

    double phi = 0;
    double WeightSum = 0;
    bool NearEdge = false;

    for (int i = 0; i < N; i++)
    {
        if (IndexList[i] != -1)
        {
            double curPhi = PixAngVector[IndexList[i]].phi;

            if (curPhi < 10 || curPhi > 350)
            {
                NearEdge = true;
            }

            if (NearEdge && curPhi < 10)
            {
                curPhi += 180;
            }

            double distance = DistanceList[i];
            phi += curPhi * (1 / distance);
            WeightSum += (1 / distance);

            if (distance < 2)
            {
                return Coords(0, PixAngVector[IndexList[i]].phi);
            }
        }
    }
    phi /= WeightSum;

    Coords X = Coords(0, phi);

    return X;
}

Coords AngleMap::FindCoordsCombi(int x, int y)
{

    Coords coords = FindCoordsPlane(x, y);

    if (coords.theta < 15)
    {
        coords.phi = FindCoordsExtraP(x, y).phi;
    }

    return coords;
}