#pragma once

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <vector>
#include <math.h>
using namespace cv;

// Finding pixels with certain colors

/// <summary>
/// Looks for pixels where the color is in range of the given upper and lower bound RGB values. Return a matrix with the corresponding pixels
/// </summary>
/// <param name="src">Source matrix containing BGR values to compare range of colors with</param>
/// <param name="lower">Scalar with RGB value of lower bound</param>
/// <param name="upper">Scalar with RGB value of upper bound</param>
/// <returns>Matrix where pixels in range of the upper and lower bound are set to 255, and pixels out of range are set to 0</returns>
Mat range(Mat src, Scalar lower, Scalar upper);

/// <summary>
/// Looks for pixels where the color is in range of the given upper and lower bound RGB values, and is equal to 0 in the mask. Return a matrix with the corresponding pixels
/// </summary>
/// <param name="src">Source matrix containing BGR values to compare range of colors with</param>
/// <param name="lower">Scalar with RGB value of lower bound</param>
/// <param name="upper">Scalar with RGB value of upper bound</param>
/// <param name="mask">Mask with values of 0 and 255. Where the mask equals 255, the output pixel will be 0 be default.
/// <returns>Matrix where pixels in range of the upper and lower bound are set to 255, and pixels out of range are set to 0</returns>
Mat range(Mat src, Scalar lower, Scalar upper, Mat& mask);

// Finding the most wanted color

/// <summary>
/// To find which color corresponds most to a given color, each pixel will get a score, the pixels with the highest scores will be put in a top 5. This can be used to find colors for the range function
/// </summary>
/// <param name="score">Calculated score of the current pixel</param>
/// <param name="color">The color of the current pixel</param>
/// <param name="scores">The top 5 scores</param>
/// <param name="colors">The colors corresponding to the top 5 scores</param>
void testScore(int score, int* color, int* scores, int colors[5][3]);

/// <summary>
/// Will try to find the color in the image which corresponds most to a given color channel. A mask could be provided to ignore certain pixels, such as blacks which, in certain cameras, will contain random colors.
/// </summary>
/// <param name="src">Matrix containing the BGR values of an image</param>
/// <param name="channel">The channel to look for, the function will try to find the color which corresponds most to this channel. 1 for red, 2 for green, 3 for blue</param>
/// <param name="mask">Mask that indicates what pixels should be ignored. The value of the pixel should be 255 to ignore the pixel.</param>
/// <returns>A scalar containing the RGB value of the found color.</returns>
Scalar scan(Mat src, int channel, Mat mask);

// Locating clusters of pixels with a similar color

/// <summary>
/// Structure for clusters of pixels of similar color. Will hold the center of the cluster and the number of pixels contained in the cluster.
/// </summary>
struct Cluster
{
    double x = 0, y = 0;
    int numberOfPixels = 1;

    Point sides[4];
    std::vector<Point> onEdge;

    bool obsolete = true;

    int Neighbors[4] = { -1, -1, -1, -1 };
    double AngleToNeighbors[4] = { NAN, NAN, NAN, NAN };

    int UnitX = 1000, UnitY = 1000;
    double theta = NAN, phi = NAN;

    bool center = false;

    std::vector<int> xs;
    std::vector<int> ys;

    Cluster(int x = 0, int y = 0, int numberOfPixels = 1) : x{ (double)x }, y{ (double)y }, numberOfPixels{ numberOfPixels } { 
        xs.push_back(x);
        ys.push_back(y);
        sides[0] = Point(x, y);
        sides[1] = Point(x, y);
        sides[2] = Point(x, y);
        sides[3] = Point(x, y);
    }
};

/// <summary>
/// Finds clusters of pixels which have similar colors. Each scalar will contain the x and y coordinate, and the number of pixels in the cluster.
/// </summary>
/// <param name="src">The black and white source containing white pixels for similar colors</param>
/// <param name="maxRadius">Maximum radius of pixels in the same cluster</param>
/// <returns>Vector containing all the clusters</returns>
std::vector<Cluster> getClusters(Mat& src, int maxRadius);

// Creating masks of pixels to ignore

#define MATCHING 1
#define LOWER 2
#define LOWEREQ 3
#define HIGHER 4
#define HIGHEREQ 5

/// <summary>
/// Compares two colors according to the given comparison type.
/// </summary>
/// <param name="one">The first color (in scalar form containing RGB value)</param>
/// <param name="two">The data array found in a matrix containing BGR data</param>
/// <param name="pos">The position of the pixel in the data array (argument two)</param>
/// <param name="filterType">The type of comparison: MATCHING, LOWER, LOWEREQ, HIGHER, HIGHEREQ</param>
/// <returns>Boolean whether the comparison is true or false</returns>
bool compColor(Scalar one, uchar* two, int pos, int filterType = 1);

/// <summary>
/// Creates a mask by comparing the colors in the matrix of BGR pixels to a given color using a certain comparison scheme
/// </summary>
/// <param name="src">Matrix containing all the BGR values of each pixel</param>
/// <param name="mask">The mask matrix where the matching pixels will be set to 255</param>
/// <param name="filter">The RGB value of the color to compare with</param>
/// <param name="filterType">The type of comparison: MATCHING, LOWER, LOWEREQ, HIGHER, HIGHEREQ</param>
void createMask(Mat src, Mat& mask, Scalar filter, int filterType = 1);

// Automatic threshold

/// <summary>
/// Calculates the mean value of a selected channel
/// </summary>
/// <param name="src">Matrix to average</param>
/// <param name="channel">Channel to average</param>
/// <returns>A double of the mean value</returns>
double meanChannel(Mat& src, int channel);

/// <summary>
/// Calculates the automatic threshold using a polynomial fit of given order
/// </summary>
/// <param name="mean">Mean value to use for the fit. Should be a mean value of the H channel</param>
/// <param name="order">Order of the polynomial fit, can be eiter 2, 3 or 4</param>
/// <returns>An integer of the chosen threshold</returns>
int thresh(double mean, int order);
int threshD(double mean);

// Wrapper function for finding clusters of a given color

/// <summary>
/// Wrapper function which executes multiple function from this library to get the clusters with colors matching to the given channel
/// </summary>
/// <param name="src">Matrix containing all the BGR values of each pixel</param>
/// <param name="channel">The channel to look for, the function will try to find the color which corresponds most to this channel. 1 for red, 2 for green, 3 for blue</param>
/// <param name="lowerThreshold">Value to be subtracted from found RGB value to get lower bound</param>
/// <param name="upperThreshold">Value to be added to found RGB value to get upper bound</param>
/// <param name="maxClusterRadius">Maximum radius for a pixel to be from the center of a cluster</param>
/// <param name="mask">Mask that indicates what pixels should be ignored. The value of the pixel should be 255 to ignore the pixel.</param>
/// <returns>Vector containing all the found clusters</returns>
std::vector<Cluster> detectColor(Mat src, int channel, int lowerThreshold, int upperThreshold, int maxClusterRadius, Mat mask);

/// <summary>
/// Converts a scalar in RGB to a scalar in HSV
/// </summary>
/// <param name="RGB">Scalar with values (R,G,B)</param>
/// <returns>Scalar with values (H,S,V)</returns>
Scalar RGBtoHSV(Scalar& RGB);

/// <summary>
/// Wrapper function which executes multiple function from this library to get the clusters with colors matching to the given channel
/// </summary>
/// <param name="src">Matrix containing all the BGR values of each pixel</param>
/// <param name="channel">The channel to look for, the function will try to find the color which corresponds most to this channel. 1 for red, 2 for green, 3 for blue</param>
/// <param name="saturationThreshold">Value subtracted from the default lower bound for saturation, the default lower bound is 100</param>
/// <param name="maxClusterRadius">Maximum radius for a pixel to be from another pixel</param>
/// <param name="mask">Mask that indicates what pixels should be ignored. The value of the pixel should be 255 to ignore the pixel.</param>
/// <returns>Vector containing all the found clusters</returns>
std::vector<Cluster> detectColor2(Mat src, int channel, int saturationThreshold, int maxClusterRadius, Mat mask);

// Wrapper function for locating LED

/// <summary>
/// Wrapper function which subtracts two images, with LED on and LED off, and detects the LED using the result.
/// </summary>
/// <param name="srcOn">Image with LED on</param>
/// <param name="srcOff">Image with LED off</param>
/// <param name="channel">The channel to look for, the function will try to find the color which corresponds most to this channel. 1 for red, 2 for green, 3 for blue</param>
/// <param name="saturationThreshold">Value subtracted from the default lower bound for saturation, the default lower bound is 100</param>
/// <param name="maxClusterRadius">Maximum radius for a pixel to be from another pixel to be in the same cluster</param>
/// <returns>A point (x,y) of the pixel location of the center of the found cluster. Prints a warning message if recognition did not went well.</returns>
Point locateLED(Mat& srcOn, Mat& srcOff, int channel, int saturationThreshold, int maxClusterRadius);