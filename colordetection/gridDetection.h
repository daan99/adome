#pragma once

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>

#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
//#include <unistd.h>

#include "colorDetection.h"
#include "VectorToFile.h"

struct PixAng
{
	int x = 0, y = 0;
	double theta = 0, phi = 0;
	bool center = 0;

	PixAng(){};
	PixAng(int x, int y, double Theta, double Phi, bool center = false) : x{x}, y{y}, theta{Theta}, phi{Phi}, center{center} {};
};

class GridDetector
{
private:
	std::vector<Cluster> BlueScaled;
	std::vector<Cluster> RedScaled;
	std::vector<Cluster> FullScaled;

	std::vector<Cluster> FullU;

	std::vector<int> AlreadyUsedIndeces;

	int DistanceAcc = 3;

	/// <summary>
	/// Private function for finding the closest dot located in the (approximate) northern direction
	/// </summary>
	/// <param name="CurrentIndex">The index of which it will try and find the northern neighbor</param>
	/// <returns>the index of the northern neighbor</returns>
	int FindNorthNeighbor(int CurrentIndex);

	/// <summary>
	/// Private function for finding the closest dot located in the (approximate) southern direction
	/// </summary>
	/// <param name="CurrentIndex">The index of which it will try and find the southern neighbor</param>
	/// <returns>the index of the southern neighbor</returns>
	int FindSouthNeighbor(int CurrentIndex);

	/// <summary>
	/// Private function for finding the closest dot located in the (approximate) eastern direction
	/// </summary>
	/// <param name="CurrentIndex">The index of which it will try and find the eastern neighbor</param>
	/// <returns>the index of the eastern neighbor</returns>
	int FindEastNeighbor(int CurrentIndex);

	/// <summary>
	/// Private function for finding the closest dot located in the (approximate) western direction
	/// </summary>
	/// <param name="CurrentIndex">The index of which it will try and find the western neighbor</param>
	/// <returns>the index of the western neighbor</returns>
	int FindWestNeighbor(int CurrentIndex);

	/// <summary>
	/// Find the 4 closest dots near the dot with index 'CurrentIndex'
	/// </summary>
	/// <param name="CurrentIndex">The index of which it will try and find the 4 nearest neighbors</param>
	/// <returns>vector containing the indeces of the 4 nearest neighbors</returns>
	std::vector<int> Find4NN(int CurrentIndex);

public:
	GridDetector(std::vector<Cluster> BlueDots, std::vector<Cluster> RedDots) : BlueGrid{BlueDots}, RedGrid{RedDots}
	{
		CenterIndexRed = FindCenterIndex();

		BlueScaled = NormalizeToUnitstep(BlueGrid);
		RedScaled = NormalizeToUnitstep(RedGrid);

		FullScaled = RedScaled;
		FullScaled.insert(FullScaled.end(), BlueScaled.begin(), BlueScaled.end());

		FullGrid = RedGrid;
		FullGrid.insert(FullGrid.end(), BlueGrid.begin(), BlueGrid.end());

		CenterIndexFull = CenterIndexRed;
	}

	std::vector<Cluster> BlueGrid;
	std::vector<Cluster> RedGrid;
	std::vector<Cluster> FullGrid;

	int CenterIndexRed;
	int UnitStep;

	int CenterIndexFull;

	/*
	 * These are camera grid and setup specific
	 */
	double CameraHeight = 27; // In mm
	double GridWidth = 3.55;  // In mm

	double k = 0;

	/// <summary>
	/// Finds the center dot of the red grid, this will later be used as the center of which the grid will be created from
	/// Also finds the unitstep to which the grids will be scaled later
	/// </summary>
	/// <returns>Integer index which gives the index of center dot of RedGrid </returns>
	int FindCenterIndex();

	/// <summary>
	/// Normalize grids to the unitstep found by 'FindCenterIndex()'
	/// </summary>
	/// <param name="Grid">Vector containing dots found by a cluster detection algorithm</param>
	/// <returns>Vector containing the dots on a scaled x and y axis</returns>
	std::vector<Cluster> NormalizeToUnitstep(std::vector<Cluster> Grid);

	/// <summary>
	/// Try to find the first order distortion coefficient, to make the fitting of square grid later more accurate
	/// </summary>
	/// <param name="MethodNumber">This number sets the method that is gonna be used to find the distortion coefficient</param>
	/// <returns>first order distortion coefficient based on one of the four methods</returns>
	double EstimateDistortionCoefficient(int MethodNumber = 0);

	/// <summary>
	/// Apply an inverse distortion on the grid FullScaled, and store it in FullU
	/// </summary>
	/// <param name="K">First order distortion coefficient used for the inverse distortion</param>
	void CorrectForDistortion(double K);

	/// <summary>
	/// Find the neighbors of the current dot, and the neighbors of its neighbors, etc, etc until all dots have been found.
	/// It will first search for a neighbor in the northern direction, once it cannot find any more to the north, will go to south, east, and west
	/// It will only go to neighbors which havent been used already to prevent an infinite recursion loop
	///
	/// This function will store the coordinates of the UnitGrid in the cluster itself and will therefore not have an return value
	/// </summary>
	/// <param name="CurrentIndex">The index of which it will try and find the neighbors</param>
	/// <param name="UnitX">X coordinate of the UnitGrid</param>
	/// <param name="UnitY">Y coordinate of the UnitGrid</param>
	void FindNeighborsRecursively(int CurrentIndex, int UnitX = 0, int UnitY = 0);

	/// <summary>
	/// Print the coordinates of the vector FullU. First the X coordinates will be printed, after which the Y coordinates will be printed
	/// </summary>
	void printUndistorted();

	/// <summary>
	/// Print the coordinates of the UnitGrid coordinates of the dots within vector FullU. First the X coordinates will be printed, after which the Y coordinates will be printed
	/// </summary>
	void printUnitGrid();

	/// <summary>
	/// Calculate theta and phi for all dots in vector FUllGrid, this is based on the coordinates in the UnitGrid
	/// </summary>
	void CalculateSpherical();

	/// <summary>
	/// Calculate the spherical coordinates theta and phi of a given pixel location, and draw this on the frame
	/// Phi and theta will be approximated using a 3 dimensional plane fit through the N nearest neighboring points
	/// </summary>
	/// <param name="Px">The index of the x coordinate within the picture</param>
	/// <param name="Py">The index of the y coordinate within the picture</param>
	/// <param name="frame">the frame on which the result will be printed</param>
	/// <returns>vector containing theta and phi in indeces 0 and 1 respectively</returns>
	std::vector<double> CalcSphericalOfPixel(int Px, int Py, cv::Mat frame);

	/// <summary>
	/// Draw the grid based on the four neighbors of each dot
	/// </summary>
	/// <param name="frame">The frame on which the grid will be drawn</param>
	void PlotGrid(cv::Mat frame);

	/// <summary>
	/// Draw circles every multiple of 10 degrees in theta (+-0.5 degrees). These circles will be drawn through the points in the grid
	/// </summary>
	/// <param name="frame">The frame on which the circles will be drawn</param>
	void DrawAngleMarkingLines(cv::Mat frame);

	/// <summary>
	/// Store a vector with all the location of the dots with their spherical coordinates in a file called AngleMap
	/// </summary>
	void StoreInFile();
};

/// <summary>Class for storing a combination of theta and phi</summary>
///
struct Coords
{
	double theta = 0.0;
	double phi = 0.0;

	Coords(double theta, double phi) : theta{theta}, phi{phi} { ; }
};
