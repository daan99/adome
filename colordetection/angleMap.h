#pragma once

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>

#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
//#include <unistd.h>

#include "gridDetection.h"
#include "colorDetection.h"
#include "VectorToFile.h"

class AngleMap
{
private:
    std::vector<PixAng> PixAngVector;

public:
    AngleMap(const char *fileName)
    {
        loadVect(fileName, PixAngVector);
    };

    int CenterIndex = -1;

    /// <summary>
    /// Draw the center point stored in the anglemap on the frame
    /// </summary>
    ///
    /// <param name="frame"> the frame on which the center point will be drawn</param>
    void DrawCenter(cv::Mat frame);

    /// <summary>
    /// Draw all points stored in the anglemap on the frame
    /// </summary>
    ///
    /// <param name="frame"> the frame on which the point will be drawn </param>
    void DrawReferencePoints(cv::Mat frame);

    /// <summary>
    /// Calculate the spherical coordinates theta and phi of a given pixel location
    /// Phi and theta will be approximated using a 3 dimensional plane fit through the N nearest neighboring points
    /// Phi and theta will be found using the point in PixAngVector of AngleMap
    /// </summary>
    /// <param name="x">The index of the x coordinate within the picture</param>
    /// <param name="y">The index of the y coordinate within the picture</param>
    /// <returns>Coords containing phi and theta</returns>
    Coords FindCoordsPlane(int x, int y);

    /// <summary>
    /// Wrapper for FindCoordsPlane using the Point type instead of the two integers x and y
    /// </summary>
    ///
    /// <param name="point">Containing the x and y coordinate within of a location within the picture</param>
    /// <returns>Coords containing phi and theta</returns>
    Coords FindCoordsPlane(Point point);

    /// <summary>
    /// Calculate the spherical coordinates theta and phi of a given pixel location, using an extrapolation of the line from the center through the point (x, y)
    /// and using the N (which is a constant within the function) closest points to this line to find the PHI value of this location
    /// </summary>
    ///
    /// <param name="x">The index of the x coordinate within the picture</param>
    /// <param name="y">The index of the y coordinate within the picture</param>
    /// <returns> Coords containing the value 0 for theta, and PHI which is calculated using the extrapolation method</returns>
    Coords FindCoordsExtraP(int x, int y);

    /// <summary>
    /// Calculate the spherical coordinates theta and phi of a given pixel location, using an extrapolation of the line from the center through the point (x, y)
    /// and using the N (which is a constant within the function) closest points to this line to find the PHI value of this location
    /// and giving the ability to draw the points which are found to be closest to this line on the frame
    /// </summary>
    ///
    /// <param name="x">The index of the x coordinate within the picture</param>
    /// <param name="y">The index of the y coordinate within the picture</param>
    /// <param name="frame"> the frame on which the closest points will be drawn</param>
    /// <param name="drawLine"> boolean to decide wheter or not to draw the line used in the extrapolation and the N closest points</param>
    /// <returns> Coords containing the value 0 for theta, and PHI which is calculated using the extrapolation method</returns>
    Coords FindCoordsExtraP(int x, int y, cv::Mat frame, bool drawLine = false);

    /// <summary>
    /// Calculate the spherical coordinates theta and phi of a given pixel location
    /// using the combination of the FindCoordsPlane, and FindCoordsExtraP for achieving a higher accuracy
    /// </summary>
    ///
    /// <param name="x">The index of the x coordinate within the picture</param>
    /// <param name="y">The index of the y coordinate within the picture</param>
    /// <returns> </returns>
    Coords FindCoordsCombi(int x, int y);
};