#include "gridDetection.h"

int GridDetector::FindCenterIndex()
{

	/*1: north, 2: south, 3: east, 4: west*/

	/*
		Find an index i for which it 4 nearest neighbors are located in the 4 wind direction
	*/
	for (int i = 0; i < RedGrid.size(); i++)
	{

		// Store the distance, index, direction, and angle of the 4 nearest neighbors
		int DistanceList[] = {-1, -1, -1, -1};
		int IndexList[] = {-1, -1, -1, -1};
		int DirectionList[] = {-1, -1, -1, -1};
		double AngleList[] = {NAN, NAN, NAN, NAN};

		/*
			Compare the point with index 'i' to all other nodes to find how it lies with respect to point 'i'
		*/
		for (int j = 0; j < RedGrid.size(); j++)
		{
			// Skip when you compare the point to itself
			if (i == j)
				continue;

			int xDifference = (int)RedGrid[i].x - (int)RedGrid[j].x;
			int yDifference = (int)RedGrid[i].y - (int)RedGrid[j].y;

			int Distance = xDifference * xDifference + yDifference * yDifference;

			for (int k = 0; k < 4; k++)
			{
				/*
					Check whether the distance for i to j is smaller than any distance in the already stored list
					If so shift all nodes to the right, shifting the last one out of the array
				*/

				if (Distance < DistanceList[k] || DistanceList[k] == -1)
				{
					for (int l = 3; l >= k + 1; l--)
					{
						DistanceList[l] = DistanceList[l - 1];
						IndexList[l] = IndexList[l - 1];
						DirectionList[l] = DirectionList[l - 1];
						AngleList[l] = AngleList[l - 1];
					}
					IndexList[k] = j;
					DistanceList[k] = Distance;

					/*
						Find the direction based on the atan(dy/dx)
					*/
					int Direction = -1;
					double angle;
					if (xDifference != 0)
					{
						angle = atan(yDifference / xDifference) / M_PI * 180;
					}
					else if (xDifference == 0 && yDifference != 0)
					{
						angle = 90;
					}
					else
					{
						angle = 0;
					}

					if (angle < 10 && angle > -10)
					{
						if (xDifference > 0)
						{
							Direction = 2;
						}
						else
						{
							Direction = 3;
						}
					}
					else if ((angle > 80 && angle <= 90) || (angle >= -90 && angle < -80))
					{
						if (yDifference > 0)
						{
							Direction = 0;
						}
						else
						{
							Direction = 1;
						}
					}

					DirectionList[k] = Direction;
					AngleList[k] = angle;

					break;
				}
			}
		}

		/*
			Check whether all direction are found
		*/
		bool DirectionFound[] = {false, false, false, false};
		for (int j = 0; j < 4; j++)
		{
			if (DirectionList[j] == -1)
				continue;
			DirectionFound[DirectionList[j]] = true;
		}

		if (DirectionFound[0] && DirectionFound[1] && DirectionFound[2] && DirectionFound[3])
		{
			// North
			RedGrid[i].center = true;

			RedGrid[i].Neighbors[DirectionList[0]] = IndexList[0];
			RedGrid[i].AngleToNeighbors[DirectionList[0]] = AngleList[0];

			// South
			RedGrid[i].Neighbors[DirectionList[1]] = IndexList[1];
			RedGrid[i].AngleToNeighbors[DirectionList[1]] = AngleList[1];

			// East
			RedGrid[i].Neighbors[DirectionList[2]] = IndexList[2];
			RedGrid[i].AngleToNeighbors[DirectionList[2]] = AngleList[2];

			// West
			RedGrid[i].Neighbors[DirectionList[3]] = IndexList[3];
			RedGrid[i].AngleToNeighbors[DirectionList[3]] = AngleList[3];

			/*
				Find the unitstep based on the average of the distance of the 4 nearest neighbors
			*/
			double AvgDistanceFromCenter = 0;

			AvgDistanceFromCenter += sqrt(DistanceList[0]);
			AvgDistanceFromCenter += sqrt(DistanceList[1]);
			AvgDistanceFromCenter += sqrt(DistanceList[2]);
			AvgDistanceFromCenter += sqrt(DistanceList[3]);

			AvgDistanceFromCenter /= 4;

			UnitStep = (int)AvgDistanceFromCenter; // This is stored in the class itself

			return i;
		}
	}
	return -1; // If no center is found, the value -1 is returned
}

std::vector<Cluster> GridDetector::NormalizeToUnitstep(std::vector<Cluster> Grid)
{

	// Scale all points individually
	for (int i = 0; i < Grid.size(); i++)
	{
		Grid[i].x = (Grid[i].x - RedGrid[CenterIndexRed].x) / UnitStep;
		Grid[i].y = (Grid[i].y - RedGrid[CenterIndexRed].y) / UnitStep;
	}

	return Grid;
}

/// This method will contain a lot of duplicate code for the different methods
double GridDetector::EstimateDistortionCoefficient(int MethodNumber)
{

	// Furthest point in the red grid must approach distance of value 1
	if (MethodNumber == 0)
	{
		double FurthestDistance = 0;
		int FurthestIndex;

		for (int i = 0; i < RedScaled.size(); i++)
		{
			double Distance = (RedScaled[i].x) * (RedScaled[i].x) + (RedScaled[i].y) * (RedScaled[i].y);

			if (Distance > FurthestDistance)
			{
				FurthestDistance = Distance;
				FurthestIndex = i;
			}
		}

		// Find the nearest neighbor to this furthest point
		double NearestNeigborDistance = -1;
		int NearestNeigborIndex;

		for (int i = 0; i < RedScaled.size(); i++)
		{
			if (i == FurthestIndex)
				continue;

			double Distance = (RedScaled[i].x - RedScaled[FurthestIndex].x) * (RedScaled[i].x - RedScaled[FurthestIndex].x) + (RedScaled[i].y - RedScaled[FurthestIndex].y) * (RedScaled[i].y - RedScaled[FurthestIndex].y);

			if (Distance < NearestNeigborDistance || NearestNeigborDistance == -1)
			{
				NearestNeigborDistance = Distance;
				NearestNeigborIndex = i;
			}
		}

		// The distance between the FurthestPoint and its nearest neighbor must approach 1,
		// therefore a pincushion distortion will be applied to try to approach this

		double Distance = sqrt((RedScaled[FurthestIndex].x - RedScaled[NearestNeigborIndex].x) * (RedScaled[FurthestIndex].x - RedScaled[NearestNeigborIndex].x) + (RedScaled[FurthestIndex].y - RedScaled[NearestNeigborIndex].y) * (RedScaled[FurthestIndex].y - RedScaled[NearestNeigborIndex].y));
		double RS1 = RedScaled[FurthestIndex].x * RedScaled[FurthestIndex].x + RedScaled[FurthestIndex].y * RedScaled[FurthestIndex].y;
		double RS2 = RedScaled[NearestNeigborIndex].x * RedScaled[NearestNeigborIndex].x + RedScaled[NearestNeigborIndex].y * RedScaled[NearestNeigborIndex].y;

		double k = 0;
		while (abs(Distance - 1) > 0.0001 && k < 0.5)
		{
			k = k + 0.0000001;

			double X1u = RedScaled[FurthestIndex].x * (1 + k * RS1);
			double Y1u = RedScaled[FurthestIndex].y * (1 + k * RS1);

			double X2u = RedScaled[NearestNeigborIndex].x * (1 + k * RS2);
			double Y2u = RedScaled[NearestNeigborIndex].y * (1 + k * RS2);

			Distance = sqrt((X1u - X2u) * (X1u - X2u) + (Y1u - Y2u) * (Y1u - Y2u));
			// std::cout << "Distortion Estimation (Method 0). k = " << k << "("<<Distance << ")" << "\r";
		}
		// std::cout << std::endl;
		if (k >= 0.5)
		{
			k = 0;
			// std::cout << "Couldn't find proper distortion coefficient\n";
		}

		return k;

		// Furthest point blue grid must approach distance of value 1
	}
	else if (MethodNumber == 1)
	{
		double FurthestDistance = 0;
		int FurthestIndex = -1;

		for (int i = 0; i < FullScaled.size(); i++)
		{
			double Distance = (FullScaled[i].x) * (FullScaled[i].x) + (FullScaled[i].y) * (FullScaled[i].y);

			if (Distance > FurthestDistance)
			{
				FurthestDistance = Distance;
				FurthestIndex = i;
			}
		}

		// Find the nearest neighbor to this furthest point
		double NearestNeigborDistance = -1;
		;
		int NearestNeigborIndex = -1;

		for (int i = 0; i < FullScaled.size(); i++)
		{
			if (i == FurthestIndex)
				continue;

			double Distance = (FullScaled[i].x - FullScaled[FurthestIndex].x) * (FullScaled[i].x - FullScaled[FurthestIndex].x) + (FullScaled[i].y - FullScaled[FurthestIndex].y) * (FullScaled[i].y - FullScaled[FurthestIndex].y);

			if (Distance < NearestNeigborDistance || NearestNeigborDistance == -1)
			{
				NearestNeigborDistance = Distance;
				NearestNeigborIndex = i;
			}
		}

		// The distance between the FurthestPoint and its nearest neighbor must approach 1,
		// therefore a pincushion distortion will be applied to try to approach this

		double Distance = sqrt((FullScaled[FurthestIndex].x - FullScaled[NearestNeigborIndex].x) * (FullScaled[FurthestIndex].x - FullScaled[NearestNeigborIndex].x) + (FullScaled[FurthestIndex].y - FullScaled[NearestNeigborIndex].y) * (FullScaled[FurthestIndex].y - FullScaled[NearestNeigborIndex].y));
		double RS1 = FullScaled[FurthestIndex].x * FullScaled[FurthestIndex].x + FullScaled[FurthestIndex].y * FullScaled[FurthestIndex].y;
		double RS2 = FullScaled[NearestNeigborIndex].x * FullScaled[NearestNeigborIndex].x + FullScaled[NearestNeigborIndex].y * FullScaled[NearestNeigborIndex].y;

		double k = 0;
		while (abs(Distance - 1) > 0.01 && k < 0.5)
		{
			k = k + 0.00001;

			double X1u = FullScaled[FurthestIndex].x * (1 + k * RS1);
			double Y1u = FullScaled[FurthestIndex].y * (1 + k * RS1);

			double X2u = FullScaled[NearestNeigborIndex].x * (1 + k * RS2);
			double Y2u = FullScaled[NearestNeigborIndex].y * (1 + k * RS2);

			Distance = sqrt((X1u - X2u) * (X1u - X2u) + (Y1u - Y2u) * (Y1u - Y2u));
		}
		if (k >= 0.5)
		{
			k = 0;
			std::cout << "Couldn't find proper distortion coefficient\n";
		}

		return k;
	}
	// All red points with its direct neighbor must approach distance of value 1
	else if (MethodNumber == 2)
	{
		double k = 0;
		double CurrentError = 1000;
		double PrevError = 2000;

		// Minimize the error introduced by the distortion of the camera
		while (CurrentError < PrevError && k < 0.5)
		{
			k = k + 0.00001;

			PrevError = CurrentError;

			double RMSsum = 0;

			// All points and its nearest neighbor
			for (int i = 0; i < RedScaled.size(); i++)
			{
				double NearestNeigborDistance = -1;
				;
				int NearestNeigborIndex = -1;

				for (int j = 0; j < RedScaled.size(); j++)
				{
					// Ignore itself
					if (i == j)
						continue;

					double Distance = (RedScaled[i].x - RedScaled[j].x) * (RedScaled[i].x - RedScaled[j].x) + (RedScaled[i].y - RedScaled[j].y) * (RedScaled[i].y - RedScaled[j].y);

					if (Distance < NearestNeigborDistance || NearestNeigborDistance == -1)
					{
						NearestNeigborDistance = Distance;
						NearestNeigborIndex = j;
					}
				}

				double RS1 = (RedScaled[i].x * RedScaled[i].x) - (RedScaled[i].y * RedScaled[i].y);
				double RS2 = (RedScaled[NearestNeigborIndex].x * RedScaled[NearestNeigborIndex].x) - (RedScaled[NearestNeigborIndex].y * RedScaled[NearestNeigborIndex].y);

				double X1u = RedScaled[i].x * (1 + k * RS1);
				double Y1u = RedScaled[i].y * (1 + k * RS1);

				double X2u = RedScaled[NearestNeigborIndex].x * (1 + k * RS2);
				double Y2u = RedScaled[NearestNeigborIndex].y * (1 + k * RS2);

				double Error = (sqrt((X1u - X2u) * (X1u - X2u) + (Y1u - Y2u) * (Y1u - Y2u)) - 1);
				double RMSquare = Error * Error;
				RMSsum += RMSquare;
			}
			double RMeanS = RMSsum / RedScaled.size();
			double RootMS = sqrt(RMeanS);

			CurrentError = RootMS;
		}
		if (k > 0.5)
		{
			k = 0;
			std::cout << "Couldn't find proper distortion coefficient\n";
		}
		return k;
	}
	// All points using its direct neigbor must approach the value 1
	else if (MethodNumber == 3)
	{
		double k = 0;
		double CurrentError = 1000; // Arbitrary high start value
		double PrevError = 2000;	// Arbitrary high start value
		while (CurrentError < PrevError && k < 0.5)
		{
			// Slowly increase the distortion coefficient, and check wether the error is reduced compared to the previous coefficient
			k = k + 0.00001;

			PrevError = CurrentError;

			double RMSsum = 0;
			for (int i = 0; i < FullScaled.size(); i++)
			{
				double NearestNeigborDistance = -1;
				int NearestNeigborIndex = -1;

				for (int j = 0; j < FullScaled.size(); j++)
				{
					if (i == j)
						continue;

					double Distance = (FullScaled[i].x - FullScaled[j].x) * (FullScaled[i].x - FullScaled[j].x) + (FullScaled[i].y - FullScaled[j].y) * (FullScaled[i].y - FullScaled[j].y);

					if (Distance < NearestNeigborDistance || NearestNeigborDistance == -1)
					{
						NearestNeigborDistance = Distance;
						NearestNeigborIndex = j;
					}
				}

				double RS1 = (FullScaled[i].x * FullScaled[i].x) - (FullScaled[i].y * FullScaled[i].y);
				double RS2 = (FullScaled[NearestNeigborIndex].x * FullScaled[NearestNeigborIndex].x) - (FullScaled[NearestNeigborIndex].y * FullScaled[NearestNeigborIndex].y);

				double X1u = FullScaled[i].x * (1 + k * RS1);
				double Y1u = FullScaled[i].y * (1 + k * RS1);

				double X2u = FullScaled[NearestNeigborIndex].x * (1 + k * RS2);
				double Y2u = FullScaled[NearestNeigborIndex].y * (1 + k * RS2);

				double Error = (sqrt((X1u - X2u) * (X1u - X2u) + (Y1u - Y2u) * (Y1u - Y2u)) - 1);
				double RMSquare = Error * Error;
				RMSsum += RMSquare;
			}
			double RMeanS = RMSsum / FullScaled.size();
			double RootMS = sqrt(RMeanS);

			CurrentError = RootMS;
		}
		if (k >= 0.5)
		{
			k = 0;
			std::cout << "Couldn't find proper distortion coefficient\n";
		}
		return k;
	}
	// MethodNumber is out of range, thus no estimation will be done.
	else
	{
		std::cout << "There is no method with MethodNumber '" << MethodNumber << "'. No estimation has been done!" << std::endl;
		return 0;
	}
}

void GridDetector::CorrectForDistortion(double K)
{
	FullU = FullScaled;

	for (int i = 0; i < FullU.size(); i++)
	{
		double RS = FullU[i].x * FullU[i].x + FullU[i].y * FullU[i].y;

		FullU[i].x = FullU[i].x * (1 + K * RS);
		FullU[i].y = FullU[i].y * (1 + K * RS);
	}
}

void GridDetector::printUndistorted()
{
	std::cout << "XU \n";
	for (int i = 0; i < FullU.size(); i++)
	{
		std::cout << FullU[i].x << ",";
	}

	std::cout << "\n\nYU \n";
	for (int i = 0; i < FullU.size(); i++)
	{
		std::cout << FullU[i].y << ",";
	}

	std::cout << "\n\n\n"
			  << std::endl;
}

void GridDetector::FindNeighborsRecursively(int CurrentIndex, int UnitX, int UnitY)
{
	int North, South, East, West;
	FullU[CurrentIndex].UnitX = UnitX;
	FullU[CurrentIndex].UnitY = UnitY;
	FullGrid[CurrentIndex].UnitX = UnitX;
	FullGrid[CurrentIndex].UnitY = UnitY;

	// Check whether there is already a northern neigbor found
	if (FullU[CurrentIndex].Neighbors[0] == -1)
	{
		// if not: find the northern neighbor and fill it in
		North = FindNorthNeighbor(CurrentIndex);
		FullU[CurrentIndex].Neighbors[0] = North;
		FullGrid[CurrentIndex].Neighbors[0] = North;
	}
	else
	{
		North = FullU[CurrentIndex].Neighbors[0];
	}

	// Check whether there is already a southern neigbor found
	if (FullU[CurrentIndex].Neighbors[1] == -1)
	{
		// if not: find the southern neighbor and fill it in
		South = FindSouthNeighbor(CurrentIndex);
		FullU[CurrentIndex].Neighbors[1] = South;
		FullGrid[CurrentIndex].Neighbors[1] = South;
	}
	else
	{
		South = FullU[CurrentIndex].Neighbors[1];
	}

	// Check whether there is already a Eastern neigbor found
	if (FullU[CurrentIndex].Neighbors[2] == -1)
	{
		// if not: find the eastern neighbor and fill it in
		East = FindEastNeighbor(CurrentIndex);
		FullU[CurrentIndex].Neighbors[2] = East;
		FullGrid[CurrentIndex].Neighbors[2] = East;
	}
	else
	{
		East = FullU[CurrentIndex].Neighbors[2];
	}

	// Check whether there is already a western neigbor found
	if (FullU[CurrentIndex].Neighbors[3] == -1)
	{
		// if not: find the western neighbor and fill it in
		West = FindWestNeighbor(CurrentIndex);
		FullU[CurrentIndex].Neighbors[3] = West;
		FullGrid[CurrentIndex].Neighbors[3] = West;
	}
	else
	{
		West = FullU[CurrentIndex].Neighbors[3];
	}

	// When the northern neighbor of A is found, A is the southern neighbor of this found neighbor
	if (FullU[North].Neighbors[1] == -1)
	{
		FullU[North].Neighbors[1] = CurrentIndex;
		FullGrid[North].Neighbors[1] = CurrentIndex;
	}

	// When the southern neighbor of A is found, A is the northern neighbor of this found neighbor
	if (FullU[South].Neighbors[0] == -1)
	{
		FullU[South].Neighbors[0] = CurrentIndex;
		FullGrid[South].Neighbors[0] = CurrentIndex;
	}

	// When the eastern neighbor of A is found, A is the western neighbor of this found neighbor
	if (FullU[East].Neighbors[3] == -1)
	{
		FullU[East].Neighbors[3] = CurrentIndex;
		FullGrid[East].Neighbors[3] = CurrentIndex;
	}

	// When the western neighbor of A is found, A is the eastern neighbor of this found neighbor
	if (FullU[West].Neighbors[2] == -1)
	{
		FullU[West].Neighbors[2] = CurrentIndex;
		FullGrid[West].Neighbors[2] = CurrentIndex;
	}

	AlreadyUsedIndeces.push_back(CurrentIndex);

	// Check if the node has already been used
	std::vector<int> FourNN = Find4NN(CurrentIndex);
	bool GoToNorth = true;
	for (int i = (int)AlreadyUsedIndeces.size() - 1; i >= 0; i--)
	{
		if (North == AlreadyUsedIndeces[i])
		{
			GoToNorth = false;
			break;
		}
	}
	if (GoToNorth && North != -1)
	{
		FindNeighborsRecursively(North, UnitX, UnitY + 1);
	}

	bool GoToSouth = true;
	for (int i = (int)AlreadyUsedIndeces.size() - 1; i >= 0; i--)
	{
		if (South == AlreadyUsedIndeces[i])
		{
			GoToSouth = false;
			break;
		}
	}
	if (GoToSouth && South != -1)
	{
		FindNeighborsRecursively(South, UnitX, UnitY - 1);
	}

	bool GoToEast = true;
	for (int i = (int)AlreadyUsedIndeces.size() - 1; i >= 0; i--)
	{
		if (East == AlreadyUsedIndeces[i])
		{
			GoToEast = false;
			break;
		}
	}
	if (GoToEast && East != -1)
	{
		FindNeighborsRecursively(East, UnitX + 1, UnitY);
	}

	bool GoToWest = true;
	for (int i = (int)AlreadyUsedIndeces.size() - 1; i >= 0; i--)
	{
		if (West == AlreadyUsedIndeces[i])
		{
			GoToWest = false;
			break;
		}
	}
	if (GoToWest && West != -1)
	{
		FindNeighborsRecursively(West, UnitX - 1, UnitY);
	}
}

int GridDetector::FindNorthNeighbor(int CurrentIndex)
{
	double ClosestDistance = -1;
	int ClosestIndex = -1;

	for (int i = 0; i < FullU.size(); i++)
	{
		if (i == CurrentIndex)
			continue;

		double xDifference = FullU[i].x - FullU[CurrentIndex].x;
		double yDifference = FullU[i].y - FullU[CurrentIndex].y;

		// Check wether its relative direction is approximately to the north
		double angle = atan(yDifference / xDifference) / M_PI * 180;
		if (abs(angle) > 75 && abs(angle) <= 90 && yDifference < 0)
		{
			double Distance = xDifference * xDifference + yDifference * yDifference;
			if ((Distance < ClosestDistance || ClosestDistance == -1) && Distance < DistanceAcc)
			{
				ClosestDistance = Distance;
				ClosestIndex = i;
			}
		}
	}

	return ClosestIndex;
}

int GridDetector::FindSouthNeighbor(int CurrentIndex)
{
	double ClosestDistance = -1;
	int ClosestIndex = -1;

	for (int i = 0; i < FullU.size(); i++)
	{
		if (i == CurrentIndex)
			continue;

		double xDifference = FullU[i].x - FullU[CurrentIndex].x;
		double yDifference = FullU[i].y - FullU[CurrentIndex].y;

		// Check wether its relative direction is approximately to the south
		double angle = atan(yDifference / xDifference) / M_PI * 180;
		if (abs(angle) > 75 && abs(angle) <= 90 && yDifference > 0)
		{
			double Distance = xDifference * xDifference + yDifference * yDifference;
			if ((Distance < ClosestDistance || ClosestDistance == -1) && Distance < DistanceAcc)
			{
				ClosestDistance = Distance;
				ClosestIndex = i;
			}
		}
	}

	return ClosestIndex;
}

int GridDetector::FindEastNeighbor(int CurrentIndex)
{
	double ClosestDistance = -1;
	int ClosestIndex = -1;

	for (int i = 0; i < FullU.size(); i++)
	{
		if (i == CurrentIndex)
			continue;

		double xDifference = FullU[i].x - FullU[CurrentIndex].x;
		double yDifference = FullU[i].y - FullU[CurrentIndex].y;

		// Check wether its relative direction is approximately to the East
		double angle = atan(yDifference / xDifference) / M_PI * 180;
		if (abs(angle) < 15 && xDifference > 0)
		{
			double Distance = xDifference * xDifference + yDifference * yDifference;
			if ((Distance < ClosestDistance || ClosestDistance == -1) && Distance < DistanceAcc)
			{
				ClosestDistance = Distance;
				ClosestIndex = i;
			}
		}
	}

	return ClosestIndex;
}

int GridDetector::FindWestNeighbor(int CurrentIndex)
{
	double ClosestDistance = -1;
	int ClosestIndex = -1;

	for (int i = 0; i < FullU.size(); i++)
	{
		if (i == CurrentIndex)
			continue;

		double xDifference = FullU[i].x - FullU[CurrentIndex].x;
		double yDifference = FullU[i].y - FullU[CurrentIndex].y;

		// Check wether its relative direction is approximately to the West
		double angle = atan(yDifference / xDifference) / M_PI * 180;
		if (abs(angle) < 15 && xDifference < 0)
		{
			double Distance = xDifference * xDifference + yDifference * yDifference;
			if ((Distance < ClosestDistance || ClosestDistance == -1) && Distance < DistanceAcc)
			{
				ClosestDistance = Distance;
				ClosestIndex = i;
			}
		}
	}

	return ClosestIndex;
}

std::vector<int> GridDetector::Find4NN(int CurrentIndex)
{
	double DistanceList[] = {-1, -1, -1, -1};
	int IndexList[] = {-1, -1, -1, -1};
	int i = CurrentIndex;
	/*
		Compare the point with index 'i' to all other nodes to find how it lies with respect to point 'i'
	*/
	for (int j = 0; j < FullU.size(); j++)
	{
		// Skip when you compare the point to itself
		if (i == j)
			continue;

		double xDifference = FullU[i].x - FullU[j].x;
		double yDifference = FullU[i].y - FullU[j].y;

		double Distance = xDifference * xDifference + yDifference * yDifference;
		for (int k = 0; k < 4; k++)
		{
			if (Distance < DistanceList[k] || DistanceList[k] == -1)
			{
				for (int l = 3; l >= k + 1; l--)
				{
					DistanceList[l] = DistanceList[l - 1];
					IndexList[l] = IndexList[l - 1];
				}
				IndexList[k] = j;
				DistanceList[k] = Distance;

				break;
			}
		}
	}

	// Convert to vector instead of array
	std::vector<int> FourNN;
	FourNN.push_back(IndexList[0]);
	FourNN.push_back(IndexList[1]);
	FourNN.push_back(IndexList[2]);
	FourNN.push_back(IndexList[3]);
	return FourNN;
}

void GridDetector::PlotGrid(cv::Mat frame)
{

	for (int i = 0; i < FullGrid.size(); i++)
	{

		for (int j = 0; j < 4; j++)
		{
			// Draw line between point and all its neighbors
			int NeighborIndex = FullU[i].Neighbors[j];
			if (NeighborIndex != -1)
				cv::line(frame, cv::Point((int)FullGrid[i].x, (int)FullGrid[i].y), cv::Point((int)FullGrid[NeighborIndex].x, (int)FullGrid[NeighborIndex].y), cv::Scalar(0, 0, 0), 2);
		}
	}
}

void GridDetector::printUnitGrid()
{
	std::cout << "UnitX \n";
	for (int i = 0; i < FullU.size(); i++)
	{
		std::cout << FullU[i].UnitX << ",";
	}

	std::cout << "\n\nUnitY \n";
	for (int i = 0; i < FullU.size(); i++)
	{
		std::cout << FullU[i].UnitY << ",";
	}

	std::cout << "\n\n\n"
			  << std::endl;
}

void GridDetector::CalculateSpherical()
{

	for (int i = 0; i < FullGrid.size(); i++)
	{
		double x = FullGrid[i].UnitX * GridWidth;
		double y = FullGrid[i].UnitY * GridWidth;
		double z = CameraHeight;

		FullGrid[i].theta = atan((sqrt(x * x + y * y)) / (z)) / M_PI * 180;

		if (y == 0)
		{
			FullGrid[i].phi = 0;
		}
		else if (x == 0 && y != 0)
		{
			FullGrid[i].phi = 90;
		}
		else
		{
			FullGrid[i].phi = atan(y / x) / M_PI * 180;
		}
	}
}

std::vector<double> GridDetector::CalcSphericalOfPixel(int Px, int Py, cv::Mat frame)
{

	int N = 5;

	std::vector<double> DistanceList;
	std::vector<int> IndexList;
	for (int i = 0; i < N; i++)
	{
		DistanceList.push_back(-1);
		IndexList.push_back(-1);
	}

	/*
		Compare the point with index 'i' to all other nodes to find how it lies with respect to point 'i'
	*/
	for (int i = 0; i < FullGrid.size(); i++)
	{
		// Skip when you compare the point to itself
		if (FullGrid[i].center)
		{
			cv::drawMarker(frame, cv::Point((int)FullGrid[i].x, (int)FullGrid[i].y), cv::Scalar(0, 255, 255), cv::MARKER_CROSS, 10, 2);
			cv::putText(frame, "(0, 0)", cv::Point((int)FullGrid[i].x, (int)FullGrid[i].y), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.0, cv::Scalar(255, 255, 255), 1, cv::LINE_AA);
		}

		double xDifference = FullGrid[i].x - Px;
		double yDifference = FullGrid[i].y - Py;

		double Distance = sqrt(xDifference * xDifference + yDifference * yDifference);

		for (int k = 0; k < N; k++)
		{
			if (Distance < DistanceList[k] || DistanceList[k] == -1)
			{
				for (int l = N - 1; l >= k + 1; l--)
				{
					DistanceList[l] = DistanceList[l - 1];
					IndexList[l] = IndexList[l - 1];
				}
				IndexList[k] = i;
				DistanceList[k] = Distance;

				break;
			}
		}
	}

	double theta, phi, R2;
	if (DistanceList[0] < 4)
	{
		theta = FullGrid[IndexList[0]].theta;
		phi = FullGrid[IndexList[0]].phi;
		R2 = 0;
	}
	else
	{
		// Fit a plane through the N points

		double Sx2, Sxy, Sx, Sy2, Sy, S1, SxzTheta, SxzPhi, SyzTheta, SyzPhi, SzTheta, SzPhi;
		Sx2 = Sxy = Sx = Sy2 = Sy = S1 = SxzTheta = SxzPhi = SyzTheta = SyzPhi = SzTheta = SzPhi = 0;

		for (int i = 0; i < N; i++)
		{
			Sx2 += FullGrid[IndexList[i]].x * FullGrid[IndexList[i]].x;
			Sxy += FullGrid[IndexList[i]].x * FullGrid[IndexList[i]].y;
			Sx += FullGrid[IndexList[i]].x;
			Sy2 += FullGrid[IndexList[i]].y * FullGrid[IndexList[i]].y;
			Sy += FullGrid[IndexList[i]].y;
			S1 += 1;

			SxzTheta += FullGrid[IndexList[i]].x * FullGrid[IndexList[i]].theta;
			SxzPhi += FullGrid[IndexList[i]].x * FullGrid[IndexList[i]].phi;

			SyzTheta += FullGrid[IndexList[i]].y * FullGrid[IndexList[i]].theta;
			SyzPhi += FullGrid[IndexList[i]].y * FullGrid[IndexList[i]].phi;

			SzTheta += FullGrid[IndexList[i]].theta;
			SzPhi += FullGrid[IndexList[i]].phi;
		}

		double Mat1[3][3];

		Mat1[0][0] = Sx2;
		Mat1[0][1] = Sxy;
		Mat1[0][2] = Sx;

		Mat1[1][0] = Sxy;
		Mat1[1][1] = Sy2;
		Mat1[1][2] = Sy;

		Mat1[2][0] = Sx;
		Mat1[2][1] = Sy;
		Mat1[2][2] = S1;

		double determinant = Mat1[0][0] * (Mat1[1][1] * Mat1[2][2] - Mat1[2][1] * Mat1[1][2]) + Mat1[0][1] * (Mat1[1][2] * Mat1[2][0] - Mat1[1][0] * Mat1[2][2]) + Mat1[0][2] * (Mat1[1][0] * Mat1[2][1] - Mat1[2][0] * Mat1[1][1]);

		double InvMat1[3][3];

		InvMat1[0][0] = (Mat1[1][1] * Mat1[2][2] - Mat1[1][2] * Mat1[2][1]) / determinant;
		InvMat1[0][1] = (Mat1[0][2] * Mat1[2][1] - Mat1[0][1] * Mat1[2][2]) / determinant;
		InvMat1[0][2] = (Mat1[0][1] * Mat1[1][2] - Mat1[0][2] * Mat1[1][1]) / determinant;

		InvMat1[1][0] = (Mat1[1][2] * Mat1[2][0] - Mat1[1][0] * Mat1[2][2]) / determinant;
		InvMat1[1][1] = (Mat1[0][0] * Mat1[2][2] - Mat1[0][2] * Mat1[2][0]) / determinant;
		InvMat1[1][2] = (Mat1[0][2] * Mat1[1][0] - Mat1[0][0] * Mat1[1][2]) / determinant;

		InvMat1[2][0] = (Mat1[1][0] * Mat1[2][1] - Mat1[1][1] * Mat1[2][0]) / determinant;
		InvMat1[2][1] = (Mat1[0][1] * Mat1[2][0] - Mat1[0][0] * Mat1[2][1]) / determinant;
		InvMat1[2][2] = (Mat1[0][0] * Mat1[1][1] - Mat1[0][1] * Mat1[1][0]) / determinant;

		double Atheta = InvMat1[0][0] * SxzTheta + InvMat1[0][1] * SyzTheta + InvMat1[0][2] * SzTheta;
		double Btheta = InvMat1[1][0] * SxzTheta + InvMat1[1][1] * SyzTheta + InvMat1[1][2] * SzTheta;
		double Ctheta = InvMat1[2][0] * SxzTheta + InvMat1[2][1] * SyzTheta + InvMat1[2][2] * SzTheta;

		double Aphi = InvMat1[0][0] * SxzPhi + InvMat1[0][1] * SyzPhi + InvMat1[0][2] * SzPhi;
		double Bphi = InvMat1[1][0] * SxzPhi + InvMat1[1][1] * SyzPhi + InvMat1[1][2] * SzPhi;
		double Cphi = InvMat1[2][0] * SxzPhi + InvMat1[2][1] * SyzPhi + InvMat1[2][2] * SzPhi;

		double R2Den = 0;
		double R2Num = 0;
		for (int i = 0; i < N; i++)
		{
			double zi = (FullGrid[IndexList[i]].theta) - (Atheta * FullGrid[IndexList[i]].x + Btheta * FullGrid[IndexList[i]].y + Ctheta);
			R2Den += zi * zi;

			zi = (FullGrid[IndexList[i]].theta) - (1 / N) * SzTheta;
			R2Num += zi * zi;
		}

		R2 = 1 - (R2Den / R2Num);

		theta = Atheta * Px + Btheta * Py + Ctheta;
		phi = Aphi * Px + Bphi * Py + Cphi;
	}

	std::vector<double> Coords;
	Coords.push_back(theta);
	Coords.push_back(phi);

	std::stringstream Stream1;
	Stream1 << "(" << theta << ", " << phi << ")";
	if (DistanceList[0] >= 4)
		Stream1 << "[" << R2 << "]";
	std::string StringXY = Stream1.str();

	cv::drawMarker(frame, cv::Point(Px, Py), cv::Scalar(255, 0, 255), cv::MARKER_CROSS, 10, 2);

	return Coords;
}

void GridDetector::DrawAngleMarkingLines(cv::Mat frame)
{

	for (int AngleIndex = 0; AngleIndex <= 6; AngleIndex++)
	{
		double Angle = 10 * AngleIndex;

		for (int i = 0; i < FullGrid.size(); i++)
		{

			if (abs(FullGrid[i].theta - Angle) < .5)
			{

				double RADIUS = sqrt((FullGrid[CenterIndexFull].x - FullGrid[i].x) * (FullGrid[CenterIndexFull].x - FullGrid[i].x) + (FullGrid[CenterIndexFull].y - FullGrid[i].y) * (FullGrid[CenterIndexFull].y - FullGrid[i].y));

				cv::circle(frame, cv::Point((int)FullGrid[CenterIndexFull].x, (int)FullGrid[CenterIndexFull].y), (int)RADIUS, Scalar((AngleIndex % 2) * 255, (AngleIndex % 4) * 255 / 3, (AngleIndex % 8) * 255 / 7), 1);
			}
		}
	}
}

void GridDetector::StoreInFile()
{
	std::vector<PixAng> PixelToAngleVector;

	for (int i = 0; i < FullGrid.size(); i++)
	{
		PixAng NewPixAng = PixAng(FullGrid[i].x, FullGrid[i].y, FullGrid[i].theta, FullGrid[i].phi, FullGrid[i].center);
		PixelToAngleVector.push_back(NewPixAng);
	}

	storeVect("AngleMap", PixelToAngleVector);
}
