#pragma once

#include <iostream>
#include <vector>
#include <stdlib.h>

#include "../debug/debug.h"

/// <summary>
/// Store a vector of type T in a binary file with name 'fileName'
/// </summary>
/// <param name="fileName">path to file to which the binary file will be written</param>
/// <paran name="vec">the vector of which the data will be stored</param>
template <class T>
void storeVect(const char *fileName, std::vector<T> &vec)
{
    FILE *out;
    out = fopen(fileName, "wb");
    if (out == NULL)
    {
        debugError("Failed to open data file!", 4);
        return;
    }

    unsigned int count = (unsigned int)vec.size();

    fwrite(&count, sizeof(unsigned int), 1, out);

    for (unsigned int v = 0; v < count; v++)
    {
        fwrite(&vec[v], sizeof(T), 1, out);
    }

    fclose(out);
}

/// <summary>
/// Load a vector of type T from a binary file with name 'fileName'
/// </summary>
/// <param name="fileName">path to file from which the binary file will be read</param>
/// <param name="vec">vector in which the contents of the file will be stored</param>
template <class T>
void loadVect(const char *fileName, std::vector<T> &vec)
{
    FILE *in;
    in = fopen(fileName, "rb");
    if (in == NULL)
    {
        debugError("Failed to open data file!", 4);
        return;
    }

    unsigned int count = 0;
    fread(&count, sizeof(unsigned int), 1, in);

    vec = std::vector<T>(count);

    for (unsigned int v = 0; v < count; v++)
    {

        fread(&vec[v], sizeof(T), 1, in);
    }

    fclose(in);
}