#include "colorDetection.h"

// Finding pixels with certain colors

Mat range(Mat src, Scalar lower, Scalar upper) {

    Mat dest(src.rows, src.cols, CV_8UC1);

    uchar* data = src.data;

    for (int i = 0; i < src.rows; i++) {
        for (int j = 0; j < src.cols; j++) {
            uint r = data[src.channels() * (src.cols * i + j)];
            uint g = data[src.channels() * (src.cols * i + j) + 1];
            uint b = data[src.channels() * (src.cols * i + j) + 2];

            if ((r >= lower.val[0] && r <= upper.val[0]) && (g >= lower.val[1] && g <= upper.val[1]) && (b >= lower.val[2] && b <= upper.val[2])) {
                dest.data[src.cols * i + j] = 255;
            }
            else dest.data[src.cols * i + j] = 0;
        }
    }

    return dest;
}

Mat range(Mat src, Scalar lower, Scalar upper, Mat& mask) {
    Mat dest(src.rows, src.cols, CV_8UC1);

    uchar* data = src.data;

    for (int i = 0; i < src.rows; i++) {
        for (int j = 0; j < src.cols; j++) {
            if (mask.data[(mask.cols * i + j)] == 255) {
                dest.data[src.cols * i + j] = 0;
                continue;
            }
            uint r = data[src.channels() * (src.cols * i + j)];
            uint g = data[src.channels() * (src.cols * i + j) + 1];
            uint b = data[src.channels() * (src.cols * i + j) + 2];

            if ((r >= lower.val[0] && r <= upper.val[0]) && (g >= lower.val[1] && g <= upper.val[1]) && (b >= lower.val[2] && b <= upper.val[2])) {
                dest.data[src.cols * i + j] = 255;
            }
            else dest.data[src.cols * i + j] = 0;
        }
    }

    return dest;
}

// Finding the most wanted color

void testScore(int score, int* color, int* scores, int colors[5][3]) {
    int i = 0;
    for (; i < 5 && score > scores[i]; i++) {}
    if (--i > 0) {
        for (int j = 0; j < i; j++) {
            scores[j] = scores[j + 1];
            colors[j][0] = colors[j + 1][0];
            colors[j][1] = colors[j + 1][1];
            colors[j][2] = colors[j + 1][2];
        }
        scores[i] = score;
        colors[i][0] = color[0];
        colors[i][1] = color[1];
        colors[i][2] = color[2];
    }
}

Scalar scan(Mat src, int channel, Mat mask) {
    int colors[5][3] = { 0 };
    int scores[5] = { 0 };

    uchar* data = src.data;

    for (int i = 0; i < src.rows; i++) {
        for (int j = 0; j < src.cols; j++) {

            if (mask.data[src.cols * i + j] == (uchar)255) continue;
            uint b = data[src.channels() * (src.cols * i + j)];
            uint g = data[src.channels() * (src.cols * i + j) + 1];
            uint r = data[src.channels() * (src.cols * i + j) + 2];

            int score = (channel != 1) * (255 - r) + (channel == 1) * (r)
                +(channel != 2) * (255 - g) + (channel == 2) * (g)
                +(channel != 3) * (255 - b) + (channel == 3) * (b);

            int color[3] = { (int)r, (int)g, (int)b };
            
            testScore(score, color, scores, colors);
        }
    }

    return Scalar(colors[4][0], colors[4][1], colors[4][2]);
}

// Locating clusters of pixels with similar color

void detectEdges(Cluster& cluster) {
    std::vector<int> ys;
    std::vector<std::vector<int>> xs;
    std::vector<Point> edgeCases;

    for (int i = 0; i < cluster.ys.size(); i++) {
        bool exists = false;
        for (int j = 0; j < ys.size(); j++) {
            if (ys[j] == cluster.ys[i]) {
                exists = true;
                xs[j].push_back(cluster.xs[i]);
                if (cluster.xs[i] > edgeCases[j].y) edgeCases[j].y = cluster.xs[i];
                else if (cluster.xs[i] < edgeCases[j].x) edgeCases[j].x = cluster.xs[i];
            }
        }
        if (exists) continue;

        ys.push_back(cluster.ys[i]);
        xs.push_back(std::vector<int>());
        xs[xs.size() - 1].push_back(cluster.xs[i]);
        edgeCases.push_back(Point(cluster.xs[i], cluster.xs[i]));
    }

    for (int i = 0; i < edgeCases.size(); i++) {
        cluster.onEdge.push_back(Point(edgeCases[i].x, ys[i]));
        if(edgeCases[i].x != edgeCases[i].y) 
            cluster.onEdge.push_back(Point(edgeCases[i].y, ys[i]));
    }
}

std::vector<Cluster> getClusters(Mat& src, int maxRadius) {
    std::vector<Cluster> clusters;

    // Store for each pixel the index of the corresponding cluster
    int* pixels = new int[src.rows * src.cols]{ 0 };
    //for (int p = 0; p < src.rows * src.cols; p++) pixels[p] = -1;

    for (int y = 0; y < src.rows; y++) {
        for (int x = 0; x < src.cols; x++) {
            if (src.data[src.cols * y + x] == 255 && pixels[src.cols*y+x] == 0) {
                bool partOfExistingDot = false;

                // Scroll through a radius close to the inspected pixel, if one of these pixels are in a cluster, add the pixel to the cluster
                for (int ty = (y - maxRadius < 0 ? 0 : y - maxRadius); ty < src.rows && ty <= y; ty++) {
                    for (int tx = (x - maxRadius < 0 ? 0 : x - maxRadius); tx < src.cols && tx < x + maxRadius; tx++) {

                        // Pixel found is part of cluster, and the inspected pixel is not
                        if (pixels[ty * src.cols + tx] != 0 && !partOfExistingDot) {
                            partOfExistingDot = true;
                            int index = pixels[ty * src.cols + tx]-1;

                            clusters[index].x = (clusters[index].x * clusters[index].numberOfPixels + ((double)x)) / (clusters[index].numberOfPixels + 1.0);
                            clusters[index].y = (clusters[index].y * clusters[index].numberOfPixels + ((double)y)) / (clusters[index].numberOfPixels + 1.0);
                            clusters[index].numberOfPixels++;

                            clusters[index].obsolete = (clusters[index].numberOfPixels < 5);

                            pixels[y * src.cols + x] = pixels[ty * src.cols + tx];

                            clusters[index].xs.push_back(x);
                            clusters[index].ys.push_back(y);

                            // Check sides
                            if (tx > clusters[index].sides[1].x) clusters[index].sides[1] = Point(x, y);
                            if (tx < clusters[index].sides[3].x) clusters[index].sides[3] = Point(x, y);
                            if (ty > clusters[index].sides[2].y) clusters[index].sides[2] = Point(x, y);
                            if (ty < clusters[index].sides[0].y) clusters[index].sides[0] = Point(x, y);
                        }
                        // Inspected pixel has already been added to cluster, if a pixel is found that is part of a cluster, merge the clusters
                        else if (pixels[ty * src.cols + tx] != 0 && pixels[ty * src.cols + tx] != pixels[y * src.cols + x] && !clusters[pixels[ty * src.cols + tx]-1].obsolete && partOfExistingDot) {
                            // Determine which cluster should be moved to the other cluster
                            // If at (x,y) has more pixels than at (tx,ty) => data at (tx,ty) will be moved to (x,y)
                            // (Branchless equivalent of if(at (x,y) > at (tx,ty)) newIndex = x,y; index = tx,ty; else newIndex = tx,ty; index = x,y;)
                            int newIndex = (clusters[pixels[y * src.cols + x]-1].numberOfPixels > clusters[pixels[ty * src.cols + tx]-1].numberOfPixels) * (pixels[y * src.cols + x]-1) + 
                                (clusters[pixels[y * src.cols + x]-1].numberOfPixels <= clusters[pixels[ty * src.cols + tx]-1].numberOfPixels) * (pixels[y * src.cols + x]-1);
                            int index = (clusters[pixels[y * src.cols + x]-1].numberOfPixels > clusters[pixels[ty * src.cols + tx]-1].numberOfPixels)*(pixels[ty * src.cols + tx]-1) + 
                                (clusters[pixels[y * src.cols + x]-1].numberOfPixels <= clusters[pixels[ty * src.cols + tx]-1].numberOfPixels) * (pixels[ty * src.cols + tx]-1);
                            double xMeanOld = clusters[index].x * clusters[index].numberOfPixels;
                            double yMeanOld = clusters[index].y * clusters[index].numberOfPixels;
                            
                            clusters[index].obsolete = true;

                            clusters[newIndex].x = (clusters[newIndex].x * clusters[newIndex].numberOfPixels + xMeanOld) / (clusters[newIndex].numberOfPixels + clusters[index].numberOfPixels);
                            clusters[newIndex].y = (clusters[newIndex].y * clusters[newIndex].numberOfPixels + yMeanOld) / (clusters[newIndex].numberOfPixels + clusters[index].numberOfPixels);
                            clusters[newIndex].numberOfPixels += clusters[index].numberOfPixels;

                            for (int i = 0; i < clusters[index].numberOfPixels; i++) {
                                clusters[newIndex].xs.push_back(clusters[index].xs[i]);
                                clusters[newIndex].ys.push_back(clusters[index].ys[i]);

                                pixels[clusters[index].ys[i] * src.cols + clusters[index].xs[i]] = pixels[y * src.cols + x];
                            }

                            clusters[newIndex].obsolete = (clusters[newIndex].numberOfPixels < 5);

                            if (clusters[index].sides[1].x > clusters[newIndex].sides[1].x) clusters[newIndex].sides[1] = clusters[index].sides[1];
                            if (clusters[index].sides[3].x < clusters[newIndex].sides[3].x) clusters[newIndex].sides[3] = clusters[index].sides[3];
                            if (clusters[index].sides[2].y > clusters[newIndex].sides[2].y) clusters[newIndex].sides[2] = clusters[index].sides[2];
                            if (clusters[index].sides[0].y < clusters[newIndex].sides[0].y) clusters[newIndex].sides[0] = clusters[index].sides[0];
                        }
                    }
                }

                // If no pixel is part of a cluster, create a new cluster for the pixel
                if (!partOfExistingDot) {
                    clusters.push_back(Cluster(x, y));
                    pixels[y * src.cols + x] = (int)clusters.size();
                }
            }
        }
    }

    delete[] pixels;    

    // Remove obsolete clusters i.e. clusters that have been merged or have too little member pixels
    for (int i = 0; i < clusters.size(); i++) {
        if (clusters[i].obsolete) {
            clusters.erase(clusters.begin() + i);
            i--;
        }
        else detectEdges(clusters[i]);
    }

    return clusters;

}

// Creating masks of pixels to ignore

bool compColor(Scalar one, uchar* two, int pos, int filterType) {
    uint r = (uint)two[pos + 2];
    uint g = (uint)two[pos + 1];
    uint b = (uint)two[pos];

    switch (filterType) {
    case 2:
        return (r < one[0] && g < one[1] && b < one[2]);
        break;
    case 3:
        return (r <= one[0] && g <= one[1] && b <= one[2]);
        break;
    case 4:
        return (r > one[0] && g > one[1] && b > one[2]);
        break;
    case 5:
        return (r <= one[0] && g <= one[1] && b <= one[2]);
        break;
    default:
        return (r == one[0] && g == one[1] && b == one[2]);
    }
}

void createMask(Mat src, Mat& mask, Scalar filter, int filterType) {

    for (int x = 0; x < src.rows; x++) {
        for (int y = 0; y < src.cols; y++) {
            if (compColor(filter, src.data, src.channels() * (src.cols * x + y), filterType)) {
                mask.data[src.cols * x + y] = (uchar)255;
            }
            else mask.data[src.cols * x + y] = (uchar)0;
        }
    }
}

// Automatic threshold
double meanChannel(Mat& src, int channel) {
    double m = 0;
    int num = 0;
    for (int y = 0; y < src.rows; y++) {
        for (int x = 0; x < src.cols; x++, num++) {
            m += src.data[3 * (y * src.cols + x) + channel];
        }
    }

    return m / (double)num;
}

int thresh(double mean, int order) {
    if (order == 20) {
        const double p1 = -0.024434468716538;
        const double p2 = 2.905342847689191;
        const double p3 = -17.839815058511160;


        return (int)(p1 * mean * mean + p2 * mean + p3);
    }
    else if (order == 2) {
        const double p1 = -0.026864015593968;
        const double p2 = 3.295691192274985;
        const double p3 = -32.492349964404184;


        return (int)(p1 * mean * mean + p2 * mean + p3);
    }
    else if (order == 30) {
        const double p1 = -0.0006352133344;
        const double p2 = 0.1206606463713;
        const double p3 = -7.9481015183178;
        const double p4 = 248.0591568045300;

        return (int)(p1 * mean * mean * mean + p2 * mean * mean + p3 * mean + p4);
    }
    else if (order == 4) {
        const double p1 = -0.0000032522668;
        const double p2 = -0.0004267659917;
        const double p3 = 0.1889762657730;
        const double p4 = -15.8892057271031;
        const double p5 = 470.8523040421419;

        return (int)(p1 * mean * mean * mean * mean + p2 * mean * mean * mean + p3 * mean * mean + p4 * mean + p5);
    }
    else {
        const double p1 = -0.0007815137517;
        const double p2 = 0.1521322191076;
        const double p3 = -10.1351282321118;
        const double p4 = 297.5621433958187;

        return (int)(p1 * mean * mean * mean + p2 * mean * mean + p3 * mean + p4);
    }
}


int threshD(double mean) {
    const double p1 = -0.0007815137517;
    const double p2 = 0.1521322191076;
    const double p3 = -10.1351282321118;
    const double p4 = 297.5621433958187;

    //Cast to integer since the threshold itself is an integer.
    return (int) (p1 * mean * mean * mean + p2 * mean * mean + p3 * mean + p4);
}


// Wrapper function for finding clusters of a given color

std::vector<Cluster> detectColor(Mat src, int channel, int lowerThreshold, int upperThreshold, int maxClusterRadius, Mat mask) {
    Scalar mostMatching = scan(src, channel, mask);

    Scalar lower = { mostMatching[0] - lowerThreshold, mostMatching[1] - lowerThreshold, mostMatching[2] - lowerThreshold };
    Scalar upper = { mostMatching[0] + upperThreshold, mostMatching[1] + upperThreshold, mostMatching[2] + upperThreshold };

    //std::cout << "Color matching most to channel " << channel << ": " << mostMatching << std::endl;
    //std::cout << "\tLower RGB value: " << lower << std::endl;
    //std::cout << "\tUpper RGB value: " << upper << std::endl;

    Mat matchingPixels = range(src, lower, upper);

    return getClusters(matchingPixels, maxClusterRadius);
}

Scalar RGBtoHSV(Scalar& RGB) {
    Mat rgb(1, 1, CV_8UC3, RGB);
    Mat hsv;
    cvtColor(rgb, hsv, COLOR_RGB2HSV);

    Scalar output(hsv.data[0], hsv.data[1], hsv.data[2]);

    return output;
}

std::vector<Cluster> detectColor2(Mat src, int channel, int saturationThreshold, int maxClusterRadius, Mat mask) {
    Scalar mostMatching_RGB = scan(src, channel, mask);
    Scalar mostMatching = RGBtoHSV(mostMatching_RGB);

    Scalar lowerbound = Scalar(mostMatching[0] - 40, 100 - saturationThreshold, 0);
    Scalar upperbound = Scalar(mostMatching[0] + 10, 255, 255);

    //std::cout << "Color matching most to channel " << channel << ": " << mostMatching << std::endl;
    //std::cout << "\tLower HSV value: " << lowerbound << std::endl;
    //std::cout << "\tUpper HSV value: " << upperbound << std::endl;

    Mat HSV;

    cvtColor(src, HSV, COLOR_BGR2HSV);

    Mat matchingPixels = range(HSV, lowerbound, upperbound, mask);

    return getClusters(matchingPixels, maxClusterRadius);
}

double averageIntensity(Mat& srcOn, Cluster& cluster) {
    double avgIntensity = 0;

    for (int coords = 0; coords < cluster.xs.size(); coords++) {
        int ind = 3 * (srcOn.cols * cluster.ys[coords] + cluster.xs[coords]);
        avgIntensity += (srcOn.data[ind]);
    }

    return avgIntensity / cluster.xs.size();
}

Point locateLED(Mat& srcOn, Mat& srcOff, int channel, int saturationThreshold, int maxClusterRadius) {
    Mat diff;

    // Calculates absolute difference of the on image and the off image. Due to the color of the LED in off state, the color of the LED will become negative
    absdiff(srcOn, srcOff, diff);

    // Erode the difference to remove small noises
    Mat element = getStructuringElement(MORPH_RECT,
        Size(2 * 1 + 1, 2 * 1 + 1),
        Point(1, 1));
    erode(diff, diff, element);

    // Create a mask which removes all dark colors. These colors are from small differences and should therefore be ignored
    Mat mask = Mat(diff.rows, diff.cols, CV_8UC1);
    createMask(diff, mask, Scalar(70, 70, 70), LOWEREQ);

    // Detect the clusters in the difference image
    std::vector<Cluster> clusters = detectColor2(diff, channel, saturationThreshold, maxClusterRadius, mask);

    // Check for failures
    if (clusters.size() > 1) {
        double closestDiff = 1000;
        int vClosest = 0;

        double cRatio = 0;
        int vRatio = 0;

        double highestIntensity = -1;

        for (int v = 0; v < clusters.size(); v++) {

            double rectArea = ((double)clusters[v].numberOfPixels / ((clusters[v].sides[2].y - clusters[v].sides[0].y) * (clusters[v].sides[1].x - clusters[v].sides[3].x)));
            double rectDiff = 0.785 - rectArea;

            double circleRatio = ((4.0 * 3.14 * (double)clusters[v].numberOfPixels) / ((double)clusters[v].onEdge.size() * (double)clusters[v].onEdge.size()));
            

            double avgIntensity = averageIntensity(srcOn,clusters[v]);

            putText(srcOff, std::to_string(round(avgIntensity * 100) / 100), Point((int)clusters[v].x, (int)clusters[v].y), FONT_HERSHEY_PLAIN, 0.5, Scalar(0, 0, 255));
            
            /*if (circleRatio > cRatio && circleRatio <= 1.5) {
                cRatio = circleRatio;
                vRatio = v;
                vClosest = v;
            }*/

            if (avgIntensity > highestIntensity) {
                highestIntensity = avgIntensity;
                vClosest = v;
            }

            for (int e = 0; e < clusters[v].onEdge.size(); e++) {

                drawMarker(srcOff, clusters[v].onEdge[e], Scalar(0, 0, 255, 150), MARKER_SQUARE, 1, 1);
            }
        }

        /*for (int e = 0; e < clusters[vClosest].onEdge.size(); e++) {
            
            drawMarker(srcOff, clusters[vClosest].onEdge[e], Scalar(0, 0, 255, 150), MARKER_SQUARE, 1, 1);
        }*/

        return Point((int)clusters[vClosest].x, (int)clusters[vClosest].y);
    }
    if (clusters.size() == 0) {
        //std::cout << "WARNING: No cluster detected! (-1,-1) returned!" << std::endl;
        return Point(-1, -1);
    }

    return Point((int)clusters[0].x, (int)clusters[0].y);

}