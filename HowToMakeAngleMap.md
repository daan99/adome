# How to use a new image for calibration


## Detect all coloured dots of the paper calibration grid in the image
Use detectColor2 to find the pixel location of all the dots in the calibration grid


```cpp
std::vector<Cluster> RedDots = detectColor2(CalFrame, ColourNumber, SaturationThresholdNumber, ClusterRadiusNumber, mask);
            std::vector<Cluster> BlueDots = detectColor2(CalFrame, ColourNumber, SaturationThresholdNumber, ClusterRadiusNumber, mask);
```

* CalFrame is the image in which the dots are present
* ColourNumber is the number of the color you want to detect: 1 for red, 2 for green and 3 for blue
* SaturationThresholdNumber is the number of the saturation threshold, usually regulated by a slider on the calibration window
* ClusterRadiusNumber decides how much distance there may be between pixels of the same colour while still belonging to the same cluster / dot
* mask ....

## Initialisation
Create a new GridDetector using the command given below; this class is used for finding the location of all the dots given by the (paper) calibration grid
``` cpp
GridDetector(std::vector<Cluster> BlueDots, std::vector<Cluster> RedDots)
```

## Correct for distortion
Now we want to correct for the distortion introduced by the camera to get a more accurate location of the dots on the actual grid

### Estimate the distortion coefficient

```cpp
GridDetector.EstimateDistortionCoefficient(MethodNumber)
```
* MethodNumber decides which method for estimating the distortion coefficient will be used. The different methods are described in the report[]

### Correct using the estimated distortion coefficient

```cpp
GridDetector.CorrectForDistortion(K);
```
* K is the estimated distortion coefficient

## Find the structure of the calibration grid in the picture

```cpp
GridDetector.FindNeighborsRecursively(GridDetector.CenterIndexFull);
```


## Calculate the spherical location of all the 'dots'
```cpp
GridDetector.CalculateSpherical();
```

## Store the new AngleMap

```cpp
GridDetector.StoreInFile()
```

