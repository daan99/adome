#pragma once
#define CV_WARN(message)

#include <Windows.h>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <math.h>

#include <opencv2\core\ocl.hpp>

#include "../colordetection/colorDetection.h"
#include "../colordetection/gridDetection.h"
#include "../image/editImage.h"
#include "../outputJSON/outputJSON.h"

#include "../hardwareControls/ledControl.h"

#include "../debug/debug.h"

#define RGB(r,g,b) Scalar(b,g,r)

namespace locStat {
	/// <summary>
	/// Function that locates the specified number of leds for each of the specified number of antennas. Does this by connecting to the given serial port.
	/// </summary>
	/// <param name="COM">Serial port to connect to</param>
	/// <param name="numberOfAntennas">Number of antennas to detect</param>
	/// <param name="numberOfLeds">Number of leds per antenna</param>
	/// <param name="trials">Number of maximum trials per led</param>
	/// <returns>JSON structure containing detection results</returns>
	JSON& locate(char* COM, int numberOfAntennas, int numberOfLeds, int trials, bool debugMode);

	/// <summary>
	/// Sets the function that is called when an error occurs.
	/// </summary>
	/// <param name="onExit">Function pointer of type (int)->void</param>
	void setDebugExitCallback(DebugExitCallback onExit);

	/// <summary>
	/// Function that is normally called when the process exits. Use this to stop the VideoCapture object when defining your own exit callback using `void setDebugExitCallback(int)`.
	/// </summary>
	void onExit();
}