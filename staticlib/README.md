# OCS Static library
For easier development of code using the OCS source code, it is possible to create a static library that contains all the code of OCS.

A pre-compiled version of this library is provided in the [releases](https://gitlab.com/daan99/adome/-/releases) tab, but this version is only built for Windows.

## Table of Contents
- [Building the static library](#building-the-static-library)
- [Using the static library](#using-the-static-library)

## Building the static library
When the static library has to be used on other platforms that Windows, or you have added changes to the source code, you have to rebuild the static library.

For this instruction, it is assumed that the user has access to the static OCS library and OpenCV.

To do this, make sure you have configured your project containing all the code as follows:
- The compiler is set to build a static library (`.lib`)
- Make sure the include directories, and the library directories are set correctly. (See the main README for more info)
- Make sure it compiles child libraries statically. On Windows this is done by setting `C/C++->Code Generation->Runtime Library` to `Multi-threaded (/MT)` or `Multi-threaded Debug (/MTd)` in Visual Studio.
- All `.h` files and `.cpp` files are added to the project, including the `locStat.h` and `locStat.cpp` files. These last two contain the code specific to the static library.

With these settings, a static library should be compiled.

## Using the static library
To use the static library, add the `.lib` file to your project (the pre-compiled version is named `staticOCS.lib`). Next, include the `locStat.h` in your code. All methods defined in the `locStat.h` file can be used.

In order to compile your code, make sure at least the following header files are also linked to your project:
```
opencv_core453.lib
opencv_imgcodecs453.lib
opencv_highgui453.lib
opencv_imgproc453.lib
opencv_videoio453.lib
IlmImf.lib
ippicvmt.lib
libjpeg-turbo.lib
libpng.lib
libtiff.lib
libwebp.lib
zlib.lib
ippiw.lib
ittnotify.lib
libopenjp2.lib
opencv_aruco453.lib
opencv_calib3d453.lib
opencv_flann453.lib
opencv_features2d453.lib
```

It should now be possible to compile a program that can use the methods defined in the `locStat.h` header file.
