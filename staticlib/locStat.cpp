#include "locStat.h"

namespace locStat {
    using namespace cv;

    // Start video feed
    VideoCapture cap;

    void onExit() {
        cap.release();
    }

    void takePictures(VideoCapture& capture, Mat& frame_on, Mat& frame_off, Led& led) {
        Mat img;


        led.setPower(true);
        capture >> img;
        frame_off = cropToPercentage(img, 0.80);


        led.setPower(false);
        capture >> img;
        frame_on = cropToPercentage(img, 0.80);
    }

    Point findLed(Led& led, int antennaId, int ledId, int trials = 1) {
        Mat frame_off, frame_on;

        Point LED(-1, -1);

        int trial = 0;

        for (trial = 0; trial < trials && LED == Point(-1, -1); trial++) {
            takePictures(cap, frame_on, frame_off, led);
            LED = locateLED(frame_on, frame_off, 3, 10, 5);
        }

        std::string fileNameBase = "_ant";
        fileNameBase += std::to_string(antennaId) + std::string("_led_") + std::to_string(ledId) + std::string("_trial_") + std::to_string(trial - 1);
        std::string fileNameOn = "frame_on";
        fileNameOn += fileNameBase + std::string(".jpg");
        std::string fileNameOff = "frame_off";
        fileNameOff += fileNameBase + std::string(".jpg");

        drawMarker(frame_off, LED, Scalar(255, 0, 255), MARKER_CROSS, 100, 1);
        debugSaveImage(frame_on, fileNameOn.c_str());
        debugSaveImage(frame_off, fileNameOff.c_str());

        return LED;
    }

    // INPUT ARGUMENTS: [2]
    int no_args = 4;
    char INPUTARGS[] = "\t- COM port of ADome connection\n\t- Number of antennas in the ADome\n\t- Number of leds per antenna\n\t- Number of trials";

    int handleOCVError(int status, const char* func_name,
        const char* err_msg, const char* file_name,
        int line, void* userdata)
    {
        std::cout << "Undefined error in: " << file_name << ", on line: " << line << std::endl;
        std::cout << err_msg << std::endl;

        exit(-1);

        return 0;
    }

    JSON& locate(char* COM, int numberOfAntennas, int numberOfLeds, int trials, bool debugMode)
    {
        cap = VideoCapture(0, CAP_DSHOW);
        setDebugLogging(debugMode);
        setDebugPath("./latestLocalizationRun.log");

        cv::redirectError(handleOCVError);
        debugNote("DEBUG MODE ENABLED");
        atexit(onExit);

        if (!cap.isOpened()) {
            debugError("Camera has not been opened!", 5);
        }

        if (trials < 1) {
            debugError("Number of trials should be at least equal to 1", 1);
        }

        JSON json;
        JSONArray* arr = new JSONArray;
        json.newEntry((char*)"antennas", arr);

        cap.set(CAP_PROP_FRAME_HEIGHT, 1080);
        cap.set(CAP_PROP_FRAME_WIDTH, 1920);
        cap.set(CAP_PROP_AUTO_EXPOSURE, 0.25);

        //switchExp(cap);
        cap.set(CAP_PROP_EXPOSURE, -12);

        //Led led(COM, 0, false);
        //led.setColor(Scalar(0, 0, 255));

        AngleMap angleMap("AngleMap2");

        // TODO implement looping through all leds

        for (int a = 0; a < numberOfAntennas; a++) {
            JSONAntenna* ant = new JSONAntenna(a);

            for (int l = 0; l < numberOfLeds; l++) {
                Coords* ledCoords = NULL;

                //Point ledCart = findLed(led, a, l, trials);
                Point ledCart(0, 0);
                if (ledCart != Point(-1, -1)) ledCoords = new Coords(angleMap.FindCoords(ledCart));

                ant->push(ledCoords);
            }

            arr->push(ant);
        }

        cap.release();

        return json;
    }

    // setDebugExitCallback implementation
    void setDebugExitCallback(DebugExitCallback onExit) {
        onDebugExit(onExit);
    }
}